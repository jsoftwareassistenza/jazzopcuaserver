/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serveropc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Vector;
import nucleo.Data;
import nucleo.FunzioniOpc;
import nucleo.Record;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author pgx71
 */
public class JobLetturaRobot implements Job {

    public void execute(JobExecutionContext jec) throws JobExecutionException {
        System.out.println("JOB LETTURA ROBOT INIZIO...");
        String azienda = jec.getJobDetail().getJobDataMap().getString("azienda");
        String urlserver = jec.getJobDetail().getJobDataMap().getString("server");
//        String urlserver2 = jec.getJobDetail().getJobDataMap().getString("server2");
//        String user = jec.getJobDetail().getJobDataMap().getString("user");
//        String pwd = jec.getJobDetail().getJobDataMap().getString("pwd");
//        String depgiac = jec.getJobDetail().getJobDataMap().getString("depgiac");
        System.out.println("   AZIENDA:" + azienda);
        System.out.println("   URL :" + urlserver);

        Connection connAz = null;
        if (azienda != null && !azienda.equals("")) {
            try {
                //connCom = DriverManager.getConnection(EnvOpc.connessioneJdbc + "comunejazz" + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);
                connAz = DriverManager.getConnection(EnvOpc.connessioneJdbc + azienda + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);
                boolean inserimento = false;
                int descrricetta = -1;
                int numpezzi = 0;
                int templav = 0;
                int allarmi = 0;
                int consfinitura = 0;
                int consspazzola = 0;
                Vector risposta = FunzioniOpc.leggiDati();
                String data = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                String ora = df.format(new java.util.Date());
                for (int i = 0; i < risposta.size(); i++) {
                    inserimento = true;
                    Record r = (Record) risposta.get(i);
                    switch (i) {
                        case 0:
                            descrricetta = r.leggiIntero("valore");
                            break;
                        case 1:
                            numpezzi = r.leggiIntero("valore");
                            break;
                        case 2:
                            templav = r.leggiIntero("valore");
                            break;
                        case 3:
                            allarmi = r.leggiIntero("valore");
                            break;
                        case 4:
                            consfinitura = r.leggiIntero("valore");
                            break;

                        case 5:
                            consspazzola = r.leggiIntero("valore");
                            break;
                        case 6:
                            break;
                        case 7:

                            break;
                        case 8:

                            break;
                        case 9:

                            break;

                        default:
                            break;

                    }
                }
                if (inserimento) {
                    PreparedStatement lst = connAz.prepareStatement(
                            "INSERT INTO letturerobot (lrdata,lrricetta,lrnumpezzi,lrtempo,lrallarmi,lrconsumofinitura,lrconsumospazzola) VALUES (?,?,?,?,?,?,?)");
                    lst.clearParameters();
                    lst.setString(1, data);
                    lst.setInt(2, descrricetta);
                    lst.setInt(3, numpezzi);
                    lst.setInt(4, templav);
                    lst.setInt(5, allarmi);
                    lst.setInt(6, consfinitura);
                    lst.setInt(7, consspazzola);
                    lst.execute();
                    lst.close();
                }

            } catch (Exception e) {
                System.out.println("JOB LETTURA ROBOT ERRORE");
                e.printStackTrace();
                try {
                    connAz.close();
                } catch (Exception efosa) {
                }
            } finally {
                try {
                    System.out.println("JOB LETTURA ROBOT FINE...");
                    connAz.close();

                } catch (Exception econn) {
                }
            }
        }
    }
}
