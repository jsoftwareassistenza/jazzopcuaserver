package serveropc;

import java.lang.*;
import java.net.*;
import java.io.*;

public class ServerOpc extends java.lang.Thread
{
    private boolean fine;

    public ServerOpc()
    {

		//{{INIT_CONTROLS
		//}}
	}

    public void run()
    {
        ServerSocket servsock = null;
        fine = false;

        try
        {
            // attesa connessioni mediante ServerSocket (porta 5501)
            servsock = new ServerSocket(EnvOpc.portaServer);
            while (!fine)
            {
                // attende finch� non c'è una connessione client
                Socket s = servsock.accept();
                InputStream in = s.getInputStream();
                OutputStream out = s.getOutputStream();
                // crea e lancia thread gestore intermedio
                // individa host che ha inviato la richiesta
                GestoreIntermedioOpc gi = new GestoreIntermedioOpc(in, out, s.getInetAddress().getHostAddress());
                System.out.println("richiesta da " + s.getInetAddress().getHostAddress());

                gi.start();
            }
        }
        catch (IOException e)
        {
        }
    }
		//{{DECLARE_CONTROLS
	//}}
}