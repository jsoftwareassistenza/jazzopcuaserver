package serveropc;

import java.sql.*;

public class EnvOpc {

    // funzioni invocate da postazioni remote
    static final int LETTURA_DATI = 1;
    static final int SCRITTURA_DATI = 2;
    public static String hostServer = "";
    public static int portaServer = 5500;
    public static String azienda = "";
    public static String driverJdbc = "";
    public static String connessioneJdbc = "";
    public static String userDb = "root";
    public static String pwdDb = "";
    public static boolean backupdativsterm = false;
    public static Connection connAmbiente = null;
    public static Connection connAz = null;
    public static Connection connCom = null;
    public static String propJdbc = "?autoReconnect=true&maxReconnects=20";
    public static char dec;
    public static char errDec;
    public static String nomeSO = "";
    public static String oraSchedule = "";
    public static String tipoLettura = "";

}
