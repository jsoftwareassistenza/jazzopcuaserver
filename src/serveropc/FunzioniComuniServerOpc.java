package serveropc;

import java.net.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import nucleo.DBInputStream;
import nucleo.DBOutputStream;

public class FunzioniComuniServerOpc
{

    public static Vector leggiDati()
    {
        Vector v = null;
        Socket s = null;
        DBOutputStream dbOut = null;
        DBInputStream dbIn = null;
        
        try
        {
            // richiesta stampanti
            s = attivaSocket(EnvOpc.hostServer, EnvOpc.portaServer);
            // apre streams di lettura e scrittura
            dbOut = new DBOutputStream(s.getOutputStream());
            dbIn = new DBInputStream(s.getInputStream());
            // invia richiesta
            dbOut.scriviIntero(EnvOpc.LETTURA_DATI);
            dbOut.flush();
            // attende risposta
            v = dbIn.leggiBloccoRecord(dbOut);
        }
        catch (Exception e)
        {
            System.out.println("Errore lettura stampanti" + e.getMessage());
        }            
        finally
        {
            // chiusura socket
            try
            {
                dbOut.close();
                dbIn.close();
                s.close();
            }
            catch (Exception e)
            {
                return null;
            }                
            return v;
        }            

    }

    public static final Socket attivaSocket(String host, int porta)
    {
        boolean ok = false;
        Socket s = null;
        while (!ok)
        {
            try
            {
                s = new Socket(host, porta);
                ok = true;
            }
            catch (Exception e)
            {
                //System.out.println("Errore apertura socket:" + e.getMessage());
                ok = false;
            }
        }
        return s;
    }
    
    public static final Socket attivaSocket(String host, int porta, int ntent)
    {
        boolean ok = false;
        Socket s = null;
        //int nt = ntent;
        while (!ok && ntent > 0)
        {
            try
            {
                s = new Socket(host, porta);
                ok = true;
            }
            catch (Exception e)
            {
                //System.out.println("Errore apertura socket:" + e.getMessage());
                ok = false;
                ntent--;
            }
        }
        return s;
    }

}