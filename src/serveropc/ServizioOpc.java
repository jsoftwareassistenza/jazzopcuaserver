package serveropc;


import java.sql.*;
import java.util.*;
import nucleo.*;
import org.quartz.Scheduler;

public final class ServizioOpc
{
//    public static ListaDataBases listaDb = new ListaDataBases();
//    public static final ListaTabelle listaTab = new ListaTabelle();
//    public static ListaConnessioni listaConn = new ListaConnessioni();
    public static Codice codiceConnessioni = new Codice(EnvJazz.MIN_CODICE_CONNESSIONE, EnvJazz.MAX_CODICE_CONNESSIONE);
    public static Codice codiceOperazioni = new Codice(EnvJazz.MIN_CODICE_OPERAZIONE, EnvJazz.MAX_CODICE_OPERAZIONE);
    public static Codice codiceTmp = new Codice(1, Integer.MAX_VALUE);
    public static int numeroConnessioni = 1;
    public static Connection connAmbiente = null;
    public static Connection connTmp = null;
//    public static PoolConnessioni poolConnTmp = new PoolConnessioni();
//    public static SemaforoBinario semAmb = new SemaforoBinario(1);
//    public static SemaforoBinario semIns = new SemaforoBinario(1);
//    public static SemaforoBinario semSez = new SemaforoBinario(1);
    //public static SemaforoBinario semTrasf = new SemaforoBinario(1);
    public static Hashtable tabellaLDP = new Hashtable();
//    public static BloccoOp bloccoOp = new BloccoOp();
//    public static MessaggioServer msg = new MessaggioServer();
    public static Codice codStampaSMB = new Codice(1, Integer.MAX_VALUE);
    public static boolean attivaTrasf = false;
    public static boolean attivaTrasmAuto = false;
    public static String errdoc = "";
    // *** ART62 ***
    public static Hashtable hint = new Hashtable();
    
//    public static ProcessoTrasf ptrasf = null;
//    public static ProcessoTrasmAuto ptrasmauto = null;
    public static String trasf_msg1 = "";
    public static String trasf_msg2 = "";
    public static int trasf_prog = 0;
    public static int trasf_maxval = 0;
    public static boolean trasf_in_corso = false;
    public static boolean flag_ricezione = false;
    public static boolean trasmauto_in_corso = false;
    public static boolean installazione_in_corso = false;
    public static String info_installazione_in_corso = "";
    public static int percentuale_installazione_in_corso = 0;
    public static int fase_installazione_in_corso = 0;
    public static int totfasi_installazione_in_corso = 0;
    public static boolean download_agg_in_corso = false;
    public static boolean download_agg_plugin_in_corso = false;
    
    public static boolean rictermjcloud_in_corso = false;
    public static boolean exporthubjsales_in_corso = false;
    public static boolean importhubjsales_in_corso = false;
    
    public static boolean creazione_esercizio_in_corso = false;

    public static boolean salvataggioDoc = false;
    
    public static boolean contabFattDiff = false;
    
    public static boolean bloccoSalvaDocSuFattDiff = true;
    
    public static Scheduler scheduler = null;
    
    public static String versione_jazz = "02.17.1000";
    
    public static boolean agg_v1 = false;
    
    public static boolean agg_jaccise = true;
    public static boolean agg_jmipaaf = true;
    public static boolean agg_jspesometro = true;
    public static boolean agg_jbeer = true;
    public static boolean agg_generico = true;
    public static boolean pop = false;
    
    public static boolean aggpwd = true;
    public static boolean allineaSez = false;
    
    public ServizioOpc()
    {
    
		//{{INIT_CONTROLS
		//}}
	}
		//{{DECLARE_CONTROLS
	//}}
}