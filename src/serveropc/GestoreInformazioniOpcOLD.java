package serveropc;

import com.prosysopc.ua.samples.client.SampleConsoleClient;
import java.lang.*;
import java.sql.*;
import java.util.*;
import nucleo.DBInputStream;
import nucleo.DBOutputStream;
import nucleo.Data;
import nucleo.Debug;
import nucleo.Record;

public class GestoreInformazioniOpcOLD extends java.lang.Thread
{

    DBInputStream dbIn = null;
    DBOutputStream dbOut = null;
    String ipClient = "";
    int com = 0;

    public GestoreInformazioniOpcOLD(DBInputStream dbIn, DBOutputStream dbOut, int com, String ipClient)
    {
        this.dbIn = dbIn;
        this.dbOut = dbOut;
        this.com = com;
        this.ipClient = ipClient;

        //{{INIT_CONTROLS
        //}}
    }

    public void eseguiLetturaDati()
    {
        Vector vp = new Vector();
        try
        {
            // lettura codice postazione
            
                    SampleConsoleClient d = new SampleConsoleClient();
        vp = d.leggiRisposta();
//            String codicepost = dbIn.leggiStringa();
//            if (EnvOpc.connCom.isClosed())
//            {
//                EnvOpc.connCom = DriverManager.getConnection(EnvOpc.connessioneJdbc + "trasfjazz", "root", "");
//            }
//            Statement st = EnvOpc.connCom.createStatement();
//            ResultSet rs = st.executeQuery("SELECT * FROM postazioni WHERE tipo = 0 ORDER BY codice");
//            while (rs.next())
//            {
//                vp.addElement(new Record(rs));
//            }
//            st.close();
        } catch (Exception e)
        {
            System.out.println("Errore su invio elenco postazioni: " + e.getMessage());
            vp = new Vector();
        } finally
        {
            try
            {
                dbOut.scriviBloccoRecord(vp, dbIn);
                dbOut.flush();
                dbOut.close();
                dbIn.close();
            } catch (Exception e2)
            {
                System.out.println("Errore su invio elenco postazioni: " + e2.getMessage());
            }
        }
    }

    public void eseguiElencoPostazioniUtentiFinali()
    {
        Vector vp = new Vector();
        try
        {
            // lettura codice postazione
            String codicepost = dbIn.leggiStringa();
            Statement st = EnvOpc.connCom.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM postazioni WHERE tipo = 1 AND postazcli = \'" + codicepost + "\' ORDER BY codice");
            while (rs.next())
            {
                vp.addElement(new Record(rs));
            }
            st.close();
        } catch (Exception e)
        {
            System.out.println("Errore su invio elenco postazioni utenti finali: " + e.getMessage());
            vp = new Vector();
        } finally
        {
            try
            {
                dbOut.scriviBloccoRecord(vp, dbIn);
                dbOut.flush();
                dbOut.close();
                dbIn.close();
            } catch (Exception e2)
            {
                System.out.println("Errore su invio elenco postazioni utenti finali: " + e2.getMessage());
            }
        }
    }

    public void eseguiElencoPostazioniDestinazione()
    {
        Vector vp = new Vector();
        try
        {
            // lettura codice postazione
            String codicepost = dbIn.leggiStringa();
            if (EnvOpc.connCom.isClosed())
            {
                EnvOpc.connCom = DriverManager.getConnection(EnvOpc.connessioneJdbc + "trasfjazz", "root", "");
            }
            Statement st = EnvOpc.connCom.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM connessioni WHERE codicepostorig = \'" + codicepost + "\' ORDER BY codicepostdest");
            while (rs.next())
            {
                vp.addElement(new Record(rs));
            }
            st.close();
        } catch (Exception e)
        {
            System.out.println("Errore su invio elenco postazioni destinazione: " + e.getMessage());
            vp = new Vector();
        } finally
        {
            try
            {
                dbOut.scriviBloccoRecord(vp, dbIn);
                dbOut.flush();
                dbOut.close();
                dbIn.close();
            } catch (Exception e2)
            {
                System.out.println("Errore su invio elenco postazioni destinazione: " + e2.getMessage());
            }
        }
    }

//    public void eseguiControlloFileInviato()
//    {
//        boolean esito = false;
//        try
//        {
//            String codicepostorig = dbIn.leggiStringa();
//            String codicepostdest = dbIn.leggiStringa();
//            String nomefile = dbIn.leggiStringa();
//
//            Debug.output(" * CONTROLLO FILE INVIATO");
//            Debug.output(" - postazione orig.: " + codicepostorig);
//            Debug.output(" - postazione dest.: " + codicepostdest);
//            Debug.output(" - nome file       : " + nomefile);
//            Statement st = EnvOpc.connCom.createStatement();
//            ResultSet rs = st.executeQuery("SELECT * FROM trasferimenti WHERE nomefile = \'" + nomefile + "\' AND codicepostorig = \'" + codicepostorig + "\' AND "
//                    + "codicepostdest = \'" + codicepostdest + "\'");
//            if (rs.next())
//            {
//                if (rs.getInt("trasferito") == 1)
//                {
//                    esito = true;
//                }
//            } else
//            {
//                esito = true;
//            }
//            st.close();
//        } catch (Exception e)
//        {
//            e.printStackTrace();
//            System.out.println("Errore su controllo file inviato: " + e.getMessage());
//            esito = false;
//        } finally
//        {
//            try
//            {
//                dbOut.scriviBooleano(esito);
//                dbOut.flush();
//                dbOut.close();
//                dbIn.close();
//            } catch (Exception e2)
//            {
//                System.out.println("Errore su su controllo file inviato: " + e2.getMessage());
//            }
//        }
//    }
//
//   public void eseguiLetturaNomeFileInviato()
//    {
//        String ris = "";
//        try
//        {
//            String codicepostorig = dbIn.leggiStringa();
//            String codicepostdest = dbIn.leggiStringa();
//            Debug.output(" * LETTURA NOME FILE INVIATO");
//            Debug.output(" - postazione orig.: " + codicepostorig);
//            Debug.output(" - postazione dest.: " + codicepostdest);
//            Statement st = EnvOpc.connCom.createStatement();
//            ResultSet rs = st.executeQuery("SELECT * FROM trasferimenti WHERE codicepostorig = \'" + codicepostorig + "\' AND " +
//                    "codicepostdest = \'" + codicepostdest + "\' ORDER BY id DESC");
//            if (rs.next())
//            {
//                ris = rs.getString("nomefile");
//            }
//            st.close();
//        } catch (Exception e)
//        {
//            System.out.println("Errore su lettura file inviato: " + e.getMessage());
//            ris = "";
//        } finally
//        {
//            try
//            {
//                dbOut.scriviStringa(ris);
//                dbOut.flush();
//                dbOut.close();
//                dbIn.close();
//            } catch (Exception e2)
//            {
//                System.out.println("Errore su lettura file inviato: " + e2.getMessage());
//            }
//        }
//    }
    
    public void run()
    {
        switch (com)
        {
            case EnvOpc.LETTURA_DATI:
                eseguiLetturaDati();
                break;
//            case EnvOpc.ELENCO_POSTAZIONI_DESTINAZIONE:
//                eseguiElencoPostazioniDestinazione();
//                break;
//            case EnvOpc.ELENCO_POSTAZIONI_UF:
//                eseguiElencoPostazioniUtentiFinali();
//                break;
            
        }
    }
    //{{DECLARE_CONTROLS
    //}}
}