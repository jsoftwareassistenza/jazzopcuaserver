package serveropc;

import java.sql.*;
import java.util.*;
import java.io.*;
import java.net.*;

public class InstallazioneServerOpc
{
    public static final boolean esisteServerOpcIni()
    {
        boolean esiste = false;
        try
        {
            File f = new File("env" + File.separator + "serveropc.ini");
            if (f.exists())
                esiste = true;
        }
        catch (Exception e)
        {
        }
        return esiste;
    }


    public static final String leggiIpServer()
    {
        String ip = "localhost";
        try
        {
            ip = InetAddress.getLocalHost().getHostAddress();
        }
        catch (Exception e)
        {
        }
        return ip;
    }

    public static final void creaServerOpcIni()
    {
        try
        {
            File f = new File("env");
            if (!f.exists())
                f.mkdir();
            FileOutputStream fos = new FileOutputStream("env" + File.separator + "serveropc.ini");
            fos.write(("[DRIVER JDBC]\r\n").getBytes());
            fos.write(("org.gjt.mm.mysql.Driver\r\n").getBytes());
            fos.write(("\r\n").getBytes());
            fos.write(("[CONNESSIONE JDBC]\r\n").getBytes());
            fos.write(("jdbc:mysql://" + leggiIpServer() + "/\r\n").getBytes());
            fos.close();
        }
        catch (Exception e)
        {
            System.out.println("Errore durante creazione serveropc.ini: " + e.getMessage());
        }
    }

//    public static final void creaDbOpcJazz()
//    {
//        try
//        {
//            Connection c = DriverManager.getConnection(EnvTrasf.connessioneJdbc + "mysql", "root", "");
//            Statement st = c.createStatement();
//            st.execute("CREATE DATABASE trasfjazz");
//            st.close();
//            c.close();
//            c = DriverManager.getConnection(EnvTrasf.connessioneJdbc + "trasfjazz", "root", "");
//            st = c.createStatement();
//            st.execute("create table postazioni (id int AUTO_INCREMENT primary key," +
//                "codice varchar(10) not null," +
//                "descrizione varchar(255)," +
//                "tipo int," +
//                    // 0 = punto vendita standard
//                    // 1 = utente finale
//                "clicod varchar(10)," +
//                "postazcli varchar(10)," +
//                "azcli varchar(3)," +
//                "UNIQUE (codice))");
//            st.close();
//            st = c.createStatement();
//            st.execute("create table connessioni (id int AUTO_INCREMENT primary key," +
//                "codicepostorig varchar(10) not null," +
//                "codicepostdest varchar(10) not null," +
//                "INDEX (codicepostorig)," +
//                "INDEX (codicepostdest)" +
//                ")");
//            st.close();
//            st = c.createStatement();
//            st.execute("create table trasferimenti (id int AUTO_INCREMENT primary key," +
//                "nomefile varchar(255) not null," +
//                "codicepostdest varchar(10) not null," +
//                "trasferito int," +
//                "codicepostorig varchar(10) not null," +
//                "INDEX (nomefile)," +
//                "INDEX (codicepostdest)" +
//                ")");
//            st.close();
//            c.close();
//        }
//        catch (Exception e)
//        {
//            System.out.println("Errore durante creazione db trasfjazz: " + e.getMessage());
//        }
//    }

//    public static final void aggiornaDbOpcJazz()
//    {
//        Statement st;
//        ResultSet rs;
//
//        try
//        {
//            Connection c = DriverManager.getConnection(EnvTrasf.connessioneJdbc + "trasfjazz", "root", "");
//            st = c.createStatement();
//            try
//            {
//                st = c.createStatement();
//                rs = st.executeQuery("SELECT codicepostorig FROM trasferimenti");
//                st.close();
//            }
//            catch (Exception e)
//            {
//                try
//                {
//                    st.close();
//                    st = c.createStatement();
//                    st.execute("alter table trasferimenti add codicepostorig varchar(10) default \'\'");
//                    st.close();
//                }
//                catch (Exception e2)
//                {
//                }
//            }
//            try
//            {
//                st = c.createStatement();
//                rs = st.executeQuery("SELECT tipo FROM postazioni");
//                st.close();
//            }
//            catch (Exception e)
//            {
//                try
//                {
//                    st.close();
//                    st = c.createStatement();
//                    st.execute("alter table postazioni add tipo int default 0");
//                    st.close();
//                }
//                catch (Exception e2)
//                {
//                }
//            }
//            try
//            {
//                st = c.createStatement();
//                rs = st.executeQuery("SELECT clicod FROM postazioni");
//                st.close();
//            }
//            catch (Exception e)
//            {
//                try
//                {
//                    st.close();
//                    st = c.createStatement();
//                    st.execute("alter table postazioni add clicod varchar(10) default \'\'");
//                    st.close();
//                    st = c.createStatement();
//                    st.execute("alter table postazioni add postazcli varchar(10) default \'\'");
//                    st.close();
//                    st = c.createStatement();
//                    st.execute("alter table postazioni add azcli varchar(3) default \'\'");
//                    st.close();
//                }
//                catch (Exception e2)
//                {
//                }
//            }
//            c.close();
//        }
//        catch (Exception e)
//        {
//        }
//    }

    public static final boolean esisteDataBase(String db)
    {
        boolean esiste = false;
        try
        {
            Connection c = DriverManager.getConnection(EnvOpc.connessioneJdbc + db, "root", "");
            esiste = true;
            c.close();
        }
        catch (Exception e)
        {
        }
        return esiste;
    }
}

