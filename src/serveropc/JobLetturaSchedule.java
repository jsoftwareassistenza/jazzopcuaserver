/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package serveropc;

import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.AddressSpaceException;
import com.prosysopc.ua.client.ServerListException;
import com.prosysopc.ua.client.UaClient;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import nucleo.Data;
import nucleo.Debug;
import nucleo.EnvJazz;
import nucleo.Formattazione;
import nucleo.ProcessoControlloCisterne;
import nucleo.Query;
import nucleo.QueryAggiornamentoClient;
import nucleo.Record;
import nucleo.brewmatic.BusinessLogicBrewmatic;
import nucleo.businesslogic.BusinessLogic;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import nucleo.funzStringa;
import nucleo.industria40.BusinessLogicOpc;
import nucleo.industria40.OpcUaUtil;
import org.opcfoundation.ua.application.SessionChannel;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.common.ServiceResultException;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.transport.security.SecurityMode;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author pgx71
 */
public class JobLetturaSchedule implements Job {

    public void execute(JobExecutionContext jec) throws JobExecutionException {
        String name = ManagementFactory.getRuntimeMXBean().getName();
        String namePid = name.substring(0, name.indexOf("@"));
        System.out.println("JOB LETTURA CISTERNA INIZIO..." + namePid);
        String azienda = jec.getJobDetail().getJobDataMap().getString("azienda");
        String urlserver = jec.getJobDetail().getJobDataMap().getString("server");
        String peorainizio = jec.getJobDetail().getJobDataMap().getString("peorainizio");
        String peorafine = jec.getJobDetail().getJobDataMap().getString("peorafine");
        int peintervallo = jec.getJobDetail().getJobDataMap().getInt("peintervallo");
        String peazienda = jec.getJobDetail().getJobDataMap().getString("peazienda");
        int penumripet = jec.getJobDetail().getJobDataMap().getInt("penumripet");
        int petipolettura = jec.getJobDetail().getJobDataMap().getInt("petipolettura");

        String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        String esContabile = oggi.substring(0, 4) + oggi.substring(0, 4);

        Connection connAz = null;
        HashMap<Integer, UaClient> UaClient = new HashMap();
        try {

            Data dtcanc = new Data();
            dtcanc.decrementaGiorni(5);
            File flog = new File("logOpcUa");

            if (!flog.exists()) {
                flog.mkdir();
            }

            String[] lflog = flog.list();
            for (int i = 0; i < lflog.length; i++) {
                if (lflog[i].startsWith("opcua_log_") && (lflog[i].length() == 29 || lflog[i].length() == 36)) {
                    if (lflog[i].length() == 29) {
                        if (lflog[i].substring(18, 19).equals("_")) {
                            Data dtlog = new Data(lflog[i].substring(10, 18), Data.AAAAMMGG);
                            if (dtlog.minore(dtcanc)) {
                                File fd = new File("logOpcUa" + File.separator + lflog[i]);
                                fd.delete();
                            }
                        }
                    }
                    if (lflog[i].length() == 36) {
                        if (lflog[i].substring(25, 26).equals("_")) {
                            Data dtlog = new Data(lflog[i].substring(17, 25), Data.AAAAMMGG);
                            if (dtlog.minore(dtcanc)) {
                                File fd = new File("logOpcUa" + File.separator + lflog[i]);
                                fd.delete();
                            }
                        }
                    }
                }
            }
        } catch (Exception elog) {
            elog.printStackTrace();
        }
        try {

            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            String nflog = "opcua_log_" + (new Data()).formatta(Data.AAAAMMGG) + "_" + funzStringa.rimuovi(df.format(new java.util.Date()), ":") + ".txt";
            String nflogerr = "opcua_log_errori_" + (new Data()).formatta(Data.AAAAMMGG) + "_" + funzStringa.rimuovi(df.format(new java.util.Date()), ":") + ".txt";
            System.setOut(new PrintStream(new FileOutputStream("logOpcUa" + File.separator + nflog, true)));
            System.setErr(new PrintStream(new FileOutputStream("logOpcUa" + File.separator + nflogerr, false)));

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Lancio Processo Letture... tipo " + (petipolettura == 0 ? " sala cotta o imbottigliatrice" : " cisterna"));

        try {
            //        connComune.chiudi();
//        // crea connessione a db comune utilizzata da tutta la procedura
//        connComune = new ConnessioneServer(peoperatoreid, EnvJazz.hostServer, "comunejazz",
//                EnvJazz.ACCESSO_LETTURA_SCRITTURA, true);
// chiede connessione al db ambiente
//        connAmbiente = new ConnessioneServer(peoperatoreid, EnvJazz.hostServer, "ambientejazz",
//                EnvJazz.ACCESSO_LETTURA_SCRITTURA, true);(EnvOpc.connessioneJdbc + azienda + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);
            connAz = DriverManager.getConnection(EnvOpc.connessioneJdbc + peazienda + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);
        } catch (SQLException ex) {
            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            int ntr = 0;
            int nrip = penumripet;

            while (true) {
                // attesa intervallo (minuti)

                //int minuti = peintervallo * 1000;//TEST
                int minuti = peintervallo * 60000;

                Thread.sleep(minuti);
                // controllo tempo trascorso
                //long tempo = (new java.util.Date()).getTime();
                //if (tempo - tempoprec > ((long) intervallo) * 60000)
                {
                    boolean ok = true;
                    if (!peorainizio.equals("") && !peorafine.equals("")) {
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                        String ora = df.format(new java.util.Date());
                        if (peorafine.compareTo(peorainizio) > 0) {
                            if (ora.compareTo(peorainizio) >= 0 && ora.compareTo(peorafine) <= 0) {
                                // controllo n.trasmissioni
                                if (nrip != 0) {
                                    if (ntr < nrip) {
                                        ok = true;
                                        ntr++;
                                    } else {
                                        Debug.output("Raggiunto n.massimo iterazioni");
                                        ntr = 0;
                                        ok = false;
                                        try {

                                            connAz.close();
                                            Thread.sleep(500);
                                            String cmd = "taskkill /F /PID " + namePid;
                                            Runtime.getRuntime().exec(cmd);
                                        } catch (Exception e) {
                                            Debug.output("Exception handled " + e);
                                        }
                                    }
                                } else {
                                    ok = true;
                                }
                            } else {
                                //se siamo fuori orario devo incrementare  ntr sennò mi ritrovo con n processi in attesa di lettura
                                ntr++;
                                if (ntr > nrip) {
                                    Debug.output("Fuori Orario");
                                    ntr = 0;
                                    ok = false;
                                    try {
                                        //this.interrupt();
                                        connAz.close();
                                        Thread.sleep(500);
                                        String cmd = "taskkill /F /PID " + namePid;
                                        Runtime.getRuntime().exec(cmd);
                                    } catch (Exception e) {
                                        Debug.output("Exception handled " + e);
                                    }
                                }
                                ok = false;
                            }
                        } else {
                            // orario a cavallo della mezzanotte
                            if (ora.compareTo(peorainizio) < 0 && ora.compareTo(peorafine) > 0) {
                                ok = false;
                            }
                        }
                        if (nrip != 0 && ora.compareTo(peorafine) > 0) {
                            ntr = 0;
                        }
                    }
                    if (ok) {
                        if (petipolettura == 0) {
                            if (UaClient.size() == 0) {
                                String qry = "SELECT * FROM brewmatic_macchine WHERE mprotocollo <> 0";
                                Query q = new Query(connAz, qry);
                                q.apertura();

                                Record r = q.successivo();
                                while (r != null) {
                                    UaClient.put(r.leggiIntero("mid"), null);

                                    r = q.successivo();
                                }
                                q.chiusura();
                                Data dtlog = new Data();
                                dtlog.decrementaGiorni(90);
                                Statement logSt = connAz.createStatement();
                                logSt.execute("DELETE FROM brewmatic_batch_value_out WHERE mapboutdatains < \'" + dtlog.formatta(Data.AAAA_MM_GG, "-") + "\'");
                                logSt.close();
                                System.out.println("Effettuata pulizia brewmatic_batch_value_out: data < " + dtlog.formatta(Data.GG_MM_AAAA, "/"));

                            }

                            lettura(UaClient, connAz);
                        } else {
                            letturaCisterna(connAz, esContabile);
                        }
                    }
                    //tempoprec = tempo;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
        }

        System.out.println("   AZIENDA:" + azienda);
        System.out.println("   URL :" + urlserver);

        if (azienda != null && !azienda.equals("")) {
            try {
                connAz = DriverManager.getConnection(EnvOpc.connessioneJdbc + azienda + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);

                letturaCisterna(connAz, esContabile);
            } catch (Exception e) {
                System.out.println("JOB LETTURA CISTERNA ERRORE");
                e.printStackTrace();
                try {
                    connAz.close();
                } catch (Exception efosa) {
                }
            } finally {
                try {
                    System.out.println("JOB LETTURA CISTERNA FINE...");
                    connAz.close();

                } catch (Exception econn) {
                }
            }
        }
    }

    private void letturaCisterna(Connection connAz, String esContabile) {
        String temp = "";
        String prod = "";
        String lotto = "";
        try {
//            Data dtlog = new Data();
//            dtlog.decrementaGiorni(240);
//            Statement logSt = connAz.createStatement();
//            logSt.execute("DELETE FROM cisterne_letture WHERE csdata < \'" + dtlog.formatta(Data.AAAA_MM_GG, "-") + "\'");
//            logSt.close();
//            System.out.println("Effettuata pulizia cisterne_letture: data < " + dtlog.formatta(Data.GG_MM_AAAA, "/"));

            int numMacchine = 0;
            String qry = "SELECT DISTINCT(mip) FROM cisterne INNER JOIN brewmatic_macchine ON cistmacid = brewmatic_macchine.mid ORDER BY cistcod";
            Query q1 = new Query(connAz, qry);
            q1.apertura();
            Record rq = q1.successivo();
            while (rq != null) {
                numMacchine++;
                rq = q1.successivo();
            }
            q1.chiusura();

            Record r = null;
            SessionChannel session = null;
            UaClient client = null;
            qry = "SELECT * FROM cisterne INNER JOIN brewmatic_macchine ON cistmacid = brewmatic_macchine.mid ORDER BY cistcod";

            Query qcist = new Query(connAz, qry);

            qcist.apertura();
            for (int i = 0; i < qcist.numeroRecord(); i++) {
                Record rcist = qcist.successivo();
                boolean storicizza = rcist.leggiIntero("ciststoricizza") == 1;
                int IdentifierType = rcist.leggiIntero("mapitopc");
                //proprieta se la cisterna è impostata con le letture multiple
                String prop = BusinessLogicImpostazioni.leggiProprieta(connAz, "letturamultiple", "" + rcist.leggiIntero("cistmacid"));
                if (prop.equals("S")) {
                    if (rcist.leggiIntero("mprotocollo") == 1) {
                        System.out.println("Thread MultiCisterna: -> Started connection ");
                        if (client == null) {
                            if (numMacchine == 1) {
                                //ATTENZIONE!!!!!!!!!      connessione con il plc SIEMENS AG SIMATIC S7
                                //se cong altri non funziona usare la connessione classica quella presente in businesslogicopcua
                                //come commentato sotto
                                //e gestire la tipologia del macchinario nella configurazione iniziale

                                client = new UaClient(EnvJazz.ipServerOpc + rcist.leggiStringa("mip") + ":" + rcist.leggiIntero("mporta"));
                                client.setSecurityMode(SecurityMode.NONE);
                                // *** Application Description is sent to the server
                                ApplicationDescription appDescription = new ApplicationDescription();
                                appDescription.setApplicationName(new LocalizedText("SimpleClient", Locale.ENGLISH));
                                // 'localhost' (all lower case) in the URI is converted to the actual
                                // host name of the computer in which the application is run
                                appDescription.setApplicationUri("urn:localhost:UA:SimpleClient");
                                appDescription.setProductUri("urn:prosysopc.com:UA:SimpleClient");
                                appDescription.setApplicationType(ApplicationType.Client);

                                final ApplicationIdentity identity = new ApplicationIdentity();
                                identity.setApplicationDescription(appDescription);
                                client.setApplicationIdentity(identity);

                                client.connect();
                                //println("ServerStatus: " + client.getServerStatus());
                                //DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);

//                            try {
//                                client = BusinessLogicOpc.connection(EnvJazz.ipServerOpc + rcist.leggiStringa("mip") + ":" + rcist.leggiIntero("mporta"));
//                            } catch (URISyntaxException ex) {
//                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
//                            } catch (SecureIdentityException ex) {
//                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
//                            } catch (IOException ex) {
//                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
//                            } catch (ServiceException ex) {
//                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
//                            } catch (StatusException ex) {
//                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
//                            }
                            }
                        } else if (numMacchine > 1) {
                            client = new UaClient(EnvJazz.ipServerOpc + rcist.leggiStringa("mip") + ":" + rcist.leggiIntero("mporta"));
                            client.setSecurityMode(SecurityMode.NONE);
                            // *** Application Description is sent to the server
                            ApplicationDescription appDescription = new ApplicationDescription();
                            appDescription.setApplicationName(new LocalizedText("SimpleClient", Locale.ENGLISH));
                            // 'localhost' (all lower case) in the URI is converted to the actual
                            // host name of the computer in which the application is run
                            appDescription.setApplicationUri("urn:localhost:UA:SimpleClient");
                            appDescription.setProductUri("urn:prosysopc.com:UA:SimpleClient");
                            appDescription.setApplicationType(ApplicationType.Client);

                            final ApplicationIdentity identity = new ApplicationIdentity();
                            identity.setApplicationDescription(appDescription);
                            client.setApplicationIdentity(identity);

                            client.connect();
                        }
                        ArrayList<Record> aCmdReqSerb = null;
                        ArrayList<Record> aCmdReqDati = null;
                        Record rCmdReqSerb = null;
                        Record rCmdReqDati = null;

                        aCmdReqSerb = BusinessLogicBrewmatic.ricercaMapMacchine(connAz, rcist.leggiIntero("mid"), 1, 22, 1, 1);  // mapcampo == 22 ? "CMD_SERBATOIO_REQ_DATI" 
                        aCmdReqDati = BusinessLogicBrewmatic.ricercaMapMacchine(connAz, rcist.leggiIntero("mid"), 1, 21, 1, 1); // mapcampo == 21 ? "CMD_REQ_DATI"
                        if (aCmdReqDati.size() > 0) {
                            rCmdReqDati = aCmdReqDati.get(0);
                        }
                        if (aCmdReqSerb.size() > 0) {
                            rCmdReqSerb = aCmdReqSerb.get(0);
                        }
                        //
                        System.out.println("Client isConnected " + client.isConnected());

                        if (client != null) {
                            Thread.sleep(1000);
                            int esitoCmd = 0;
                            //String codiceRicetta = "TestString"; //legame tra il macchinario e la ricetta
                            String codiceRicetta = "" + rcist.leggiIntero("cistnumero");
                            try {

                                if (rCmdReqSerb != null) {
                                    //esitoCmd = write(codiceRicetta, rCmdReqSerb.leggiStringa("mapopcuatag"), "" + rCmdReqSerb.leggiIntero("mapopcnsindex"), client, IdentifierType);
                                    esitoCmd = BusinessLogicOpc.scrittura(codiceRicetta, rCmdReqSerb.leggiStringa("mapopcuatag"), "" + rCmdReqSerb.leggiIntero("mapopcnsindex"), client, IdentifierType);
                                    System.out.println("Thread " + rcist.leggiStringa("cistcod") + " Invio richiesta al fermentatore : " + codiceRicetta);
                                    System.out.println("esitoCmd " + esitoCmd);
                                    Thread.sleep(500);
                                }
                                if (rCmdReqDati != null) {
                                    esitoCmd = BusinessLogicOpc.scrittura("true", rCmdReqDati.leggiStringa("mapopcuatag"), "" + rCmdReqDati.leggiIntero("mapopcnsindex"), client, IdentifierType);
                                    System.out.println("Thread " + rcist.leggiStringa("cistcod") + " Invio richiesta al fermentatore : true");
                                }
                            } catch (ServerListException ex) {
                                //Logger.getLogger(.class.getName()).log(Level.SEVERE, null, ex);
                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (URISyntaxException ex) {
                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (SecureIdentityException ex) {
                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IOException ex) {
                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (ServiceException ex) {
                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (StatusException ex) {
                                Logger.getLogger(ProcessoControlloCisterne.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            if ((rCmdReqSerb != null || rCmdReqDati != null) && esitoCmd == 0) {
                                //temp = " errore ";
                                System.out.println("errore " + rcist.leggiStringa("cistcod") + " Invio richiesta al fermentatore");
                                return;
                            } else {
                                Thread.sleep(2000);
                            }

                            Query qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + rcist.leggiIntero("cistmacid") + " AND maptipoio = 1 AND mapprotocollo = 1 AND maptiporeq = 0");
                            qmp.apertura();
                            if (qmp.numeroRecord() > 0) {
                                for (int j = 0; j < qmp.numeroRecord(); j++) {
                                    String risposta = "";
                                    r = qmp.successivo();
                                    try {
                                        risposta = BusinessLogicOpc.lettura(client, r.leggiStringa("mapopcuatag"), "" + r.leggiIntero("mapopcnsindex"), r.leggiIntero("maptype"), IdentifierType);
                                    } catch (ServerListException ex) {
                                        temp = " errore ";
                                        break;
                                    } catch (URISyntaxException ex) {
                                        temp = " errore ";
                                        break;
                                    } catch (ServiceException ex) {
                                        temp = " errore ";
                                        break;
                                    } catch (SecureIdentityException ex) {
                                        temp = " errore ";
                                        break;
                                    } catch (IOException ex) {
                                        temp = " errore ";
                                        break;
                                    } catch (StatusException ex) {
                                        temp = " errore ";
                                        break;
                                    } catch (AddressSpaceException ex) {
                                        temp = " errore ";
                                        break;
                                    }

                                    if (r.leggiIntero("maptype") == 1) {
                                        //String
                                    } else if (r.leggiIntero("maptype") == 2) {
                                        //int

                                    } else if (r.leggiIntero("maptype") == 3) {
                                        //bool
                                    } else if (r.leggiIntero("maptype") == 4) {
                                        //real
                                    }
                                    double moltipl = r.leggiDouble("mapmoltipl");
                                    String campo = OpcUaUtil.MAPOUT.get(r.leggiIntero("mapcampo"));
                                    switch (campo) {
                                        case OpcUaUtil.TIPO_BIRRA: //"TIPO_BIRRA":
                                            prod = "";
                                            prod = risposta;
                                            break;
                                        case OpcUaUtil.ID_RICETTA: //"ID_RICETTA":
                                            break;
                                        case OpcUaUtil.QUANTITA: //"QUANTITA":
                                            break;
                                        case OpcUaUtil.LOTTO_SFUSO: //"LOTTO_SFUSO":
                                            lotto = "";
                                            lotto = risposta;
                                            break;
                                        case OpcUaUtil.ID_ORDINE: //"ID_ORDINE":
                                            break;
                                        case OpcUaUtil.TEMPERATURA: //"TEMPERATURA":
                                            temp = "";
                                            temp = risposta;
                                            if (!risposta.equals("") && moltipl != 1) {
                                                double d = Double.parseDouble(risposta);
                                                double t = d * moltipl;
                                                temp = "" + t;
                                            }
                                            break;
                                        default:
                                            break;
                                    }

                                }
                                //System.out.println("Val " + rcist.leggiStringa("cistcod") + " = " + risposta);
                            } else {

                                temp = " cisterna non \rovata ";
                            }

                            Record rs = new Record();
                            rs.insElem("cscistid", rcist.leggiIntero("cistid"));
                            rs.insElem("cscistcod", rcist.leggiStringa("cistcod"));
                            rs.insElem("csdata", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                            rs.insElem("csora", df.format(new java.util.Date()));
                            rs.insElem("cstemp", temp);
                            rs.insElem("csprod", prod);
                            rs.insElem("cslotto", lotto);

                            QueryAggiornamentoClient q = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "cisterne_letture" + esContabile, "csid", rs);
                            q.esecuzione();

                        } else {

                            temp = " errore ";
                            return;
                        }

                    } else if (rcist.leggiIntero("mprotocollo") == 2) {

                        System.out.println("Thread MultiCisterna: -> Started connection ");

                        if (session == null && numMacchine == 1) {
                            session = BusinessLogicOpc.connection_v2(EnvJazz.ipServerOpc + rcist.leggiStringa("mip") + ":" + rcist.leggiIntero("mporta"));
                        }
                        ArrayList<Record> aCmdReqSerb = null;
                        ArrayList<Record> aCmdReqDati = null;
                        Record rCmdReqSerb = null;
                        Record rCmdReqDati = null;

                        aCmdReqSerb = BusinessLogicBrewmatic.ricercaMapMacchine(connAz, rcist.leggiIntero("mid"), 1, 22, 1, 1);  // mapcampo == 22 ? "CMD_SERBATOIO_REQ_DATI" 
                        aCmdReqDati = BusinessLogicBrewmatic.ricercaMapMacchine(connAz, rcist.leggiIntero("mid"), 1, 21, 1, 1); // mapcampo == 21 ? "CMD_REQ_DATI"
                        if (aCmdReqDati.size() > 0) {
                            rCmdReqDati = aCmdReqDati.get(0);
                        }
                        if (aCmdReqSerb.size() > 0) {
                            rCmdReqSerb = aCmdReqSerb.get(0);
                        }
                        //
                        if (session != null) {

                            int esitoCmd = 0;
                            //String codiceRicetta = "TestString"; //legame tra il macchinario e la ricetta
                            String codiceRicetta = "" + rcist.leggiIntero("cistnumero");

                            if (rCmdReqSerb != null) {
                                esitoCmd = BusinessLogicOpc.scrittura_v2(codiceRicetta, rCmdReqSerb.leggiStringa("mapopcuatag"), rCmdReqSerb.leggiIntero("mapopcnsindex"), session, rCmdReqSerb.leggiIntero("maptype"), rCmdReqSerb.leggiIntero("mapreg"), IdentifierType);

                                System.out.println("Thread " + rcist.leggiStringa("cistcod") + " Invio richiesta al fermentatore : " + codiceRicetta);
                            }
                            if (rCmdReqDati != null) {

                                esitoCmd = BusinessLogicOpc.scrittura_v2("true", rCmdReqDati.leggiStringa("mapopcuatag"), rCmdReqDati.leggiIntero("mapopcnsindex"), session, rCmdReqDati.leggiIntero("maptype"), rCmdReqDati.leggiIntero("mapreg"), IdentifierType);
                                System.out.println("Thread " + rcist.leggiStringa("cistcod") + " Invio richiesta al fermentatore : true");
                            }

                            if ((rCmdReqSerb != null || rCmdReqDati != null) && esitoCmd == 0) {
                                temp = " errore ";
                                return;
                            }

                            Query qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + rcist.leggiIntero("cistmacid") + " AND maptipoio = 1 AND mapprotocollo = 1 AND maptiporeq = 0");
                            qmp.apertura();
                            if (qmp.numeroRecord() > 0) {
                                for (int j = 0; j < qmp.numeroRecord(); j++) {
                                    r = qmp.successivo();
                                    String risposta = "";

                                    try {
                                        risposta = BusinessLogicOpc.lettura_v2(session, r.leggiStringa("mapopcuatag"), r.leggiIntero("mapopcnsindex"), IdentifierType);
                                    } catch (ServiceResultException ex) {

                                        //Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        temp = " errore ";
                                        break;
                                    }

                                    if (r.leggiIntero("maptype") == 1) {
                                        //String
                                    } else if (r.leggiIntero("maptype") == 2) {
                                        //int

                                    } else if (r.leggiIntero("maptype") == 3) {
                                        //bool
                                    } else if (r.leggiIntero("maptype") == 4) {
                                        //real
                                    }
                                    double moltipl = r.leggiDouble("mapmoltipl");
                                    String campo = OpcUaUtil.MAPOUT.get(r.leggiIntero("mapcampo"));
                                    switch (campo) {
                                        case OpcUaUtil.TIPO_BIRRA: //"TIPO_BIRRA":
                                            prod = "";
                                            prod = risposta;
                                            break;
                                        case OpcUaUtil.ID_RICETTA: //"ID_RICETTA":
                                            break;
                                        case OpcUaUtil.QUANTITA: //"QUANTITA":
                                            break;
                                        case OpcUaUtil.LOTTO_SFUSO: //"LOTTO_SFUSO":
                                            lotto = "";
                                            lotto = risposta;
                                            break;
                                        case OpcUaUtil.ID_ORDINE: //"ID_ORDINE":
                                            break;
                                        case OpcUaUtil.TEMPERATURA: //"TEMPERATURA":
                                            temp = "";
                                            temp = risposta;
                                            if (!risposta.equals("") && moltipl != 1) {
                                                double d = Double.parseDouble(risposta);
                                                double t = d * moltipl;
                                                temp = "" + t;
                                            }
                                            break;
                                        default:
                                            break;
                                    }

                                }
//                                System.out.println("Val " + rcist.leggiStringa("cistcod") + " = " + risposta);
                            } else {

                                temp = " cisterna non \rovata ";
                            }

                            Record rs = new Record();
                            rs.insElem("cscistid", rcist.leggiIntero("cistid"));
                            rs.insElem("cscistcod", rcist.leggiStringa("cistcod"));
                            rs.insElem("csdata", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                            rs.insElem("csora", df.format(new java.util.Date()));
                            rs.insElem("cstemp", temp);
                            rs.insElem("csprod", prod);
                            rs.insElem("cslotto", lotto);

                            QueryAggiornamentoClient q = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "cisterne_letture" + esContabile, "csid", rs);
                            q.esecuzione();

                        } else {

                            temp = " errore ";
                            return;
                        }

                    }
                }
            }
            qcist.chiusura();
            if (client != null) {
                client.disconnect();
                System.out.println("Thread MultiCisterna: -> Stop connection ");
            }
            if (session != null) {
                try {
                    session.close();
                    session.closeAsync();
                    System.out.println("Thread MultiCisterna: -> Stop connection ");
                } catch (ServiceResultException ex) {
                    //Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //connAz.chiudi();
            //connCom.chiudi();
        } catch (Exception ex) {
            Debug.output("Errore su ProcessoLettureCisterneOpcUa");
            ex.printStackTrace();
        }
        Debug.output("ProcessoLettureCisterneOpcUa OK");
    }

    private void lettura(HashMap<Integer, UaClient> UaClient, Connection connAz) {

        try {
//            connCom = new ConnessioneServer(operatoreId, EnvJazz.hostServer, "comunejazz",
//                    EnvJazz.ACCESSO_LETTURA_SCRITTURA, true);

            //UaClient client = null;
            SessionChannel session = null;
            Record rbatch = null;
            //controlliamo se abbiamo dei documenti da ricevere i dati dal macchinario, se non ci sonn non facciamo niente
            Query qbatch = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out WHERE mapboutstato <> 2");
            qbatch.apertura();
            rbatch = qbatch.successivo();
            while (rbatch != null) {
                int batchId = rbatch.leggiIntero("mapboutid");
                int stato = rbatch.leggiIntero("mapboutstato");
                int mId = rbatch.leggiIntero("mapboutmid");

                String qry = "SELECT * FROM brewmatic_macchine WHERE mid = " + mId;
                Query q = new Query(connAz, qry);
                q.apertura();

                Record r = q.successivo();
                if (r != null) {
                    int IdentifierType = r.leggiIntero("mapitopc");
                    if (r.leggiIntero("mprotocollo") == 1) {
                        //System.out.println("Thread JobLetturaSchedule: -> Started ");
                        String risposta = "";
                        UaClient client = UaClient.get(mId);
                        if (client == null) {
                            try {
                                client = BusinessLogicOpc.connection(EnvJazz.ipServerOpc + r.leggiStringa("mip") + ":" + r.leggiIntero("mporta"));
                            } catch (URISyntaxException ex) {
                                Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (SecureIdentityException ex) {
                                Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IOException ex) {
                                Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (ServiceException ex) {
                                Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (StatusException ex) {
                                Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            UaClient.remove(mId);
                            UaClient.put(mId, client);
                        }
                        if (client != null) {
                            //inizio lavorazione
                            if (stato != 1) {
                                //13 NR_ORDINE_INIZIO
                                //salva il momento in cui la macchina ha iniziato l'ordine salvando data e ora
                                Query qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 1 AND maptiporeq = 0 AND mapcampo = 13");
                                qmp.apertura();
                                if (qmp.numeroRecord() > 0) {

                                    Record r12 = qmp.successivo();
                                    int docnumLetto = -1;
                                    try {
                                        risposta = BusinessLogicOpc.lettura(client, r12.leggiStringa("mapopcuatag"), "" + r12.leggiIntero("mapopcnsindex"), r12.leggiIntero("maptype"), IdentifierType);
                                    } catch (ServerListException ex) {
                                        break;
                                    } catch (URISyntaxException ex) {
                                        break;
                                    } catch (ServiceException ex) {
                                        break;
                                    } catch (SecureIdentityException ex) {
                                        break;
                                    } catch (IOException ex) {
                                        break;
                                    } catch (StatusException ex) {
                                        break;
                                    } catch (AddressSpaceException ex) {
                                        break;
                                    }

                                    if (r12.leggiIntero("maptype") == 1) {
                                        //String
                                    } else if (r12.leggiIntero("maptype") == 2) {
                                        //int
                                        docnumLetto = Formattazione.estraiIntero(risposta);
                                    } else if (r12.leggiIntero("maptype") == 3) {
                                        //bool
                                    } else if (r12.leggiIntero("maptype") == 4) {
                                        //real
                                        docnumLetto = Formattazione.estraiIntero(risposta);
                                    }
                                    Query q1 = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out WHERE mapboutdocnum = " + docnumLetto + " AND mapboutstato = 0 "
                                            + "AND mapboutmid = " + mId);
                                    q1.apertura();
                                    Record r1 = q1.successivo();
                                    if (r1 != null) {
                                        aggiornaInizioLavorazione(connAz, batchId);
                                    }
                                } else {
                                    //mapcampo == 19 ? "RICETTA_START"
                                    //salva il momento in cui la macchina ha iniziato l'ordine salvando data e ora
                                    Query qm = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 1 AND maptiporeq = 0 AND mapcampo = 19");
                                    qm.apertura();
                                    boolean ricettaStart = false;
                                    if (qm.numeroRecord() > 0) {
                                        Record r11 = qm.successivo();
                                        try {
                                            risposta = BusinessLogicOpc.lettura(client, r11.leggiStringa("mapopcuatag"), "" + r11.leggiIntero("mapopcnsindex"), r11.leggiIntero("maptype"), IdentifierType);
                                        } catch (ServerListException ex) {
                                            break;
                                        } catch (URISyntaxException ex) {
                                            break;
                                        } catch (ServiceException ex) {
                                            break;
                                        } catch (SecureIdentityException ex) {
                                            break;
                                        } catch (IOException ex) {
                                            break;
                                        } catch (StatusException ex) {
                                            break;
                                        } catch (AddressSpaceException ex) {
                                            break;
                                        }

                                        if (r11.leggiIntero("maptype") == 1) {
                                            //String
                                        } else if (r11.leggiIntero("maptype") == 2) {
                                            //int

                                        } else if (r11.leggiIntero("maptype") == 3) {
                                            //bool
                                            ricettaStart = risposta.equals("true") ? true : false;
                                        } else if (r11.leggiIntero("maptype") == 4) {
                                            //real
                                        }
                                        if (!ricettaStart) {
                                            rbatch = qbatch.successivo();
                                            qm.chiusura();
                                            qmp.chiusura();
                                            continue;
                                        }
                                        if (ricettaStart) {
                                            aggiornaInizioLavorazioneDaStartOrdine(connAz, batchId, mId, client, IdentifierType, 1, null);
                                        }
                                    }
                                    qm.chiusura();
                                }
                                qmp.chiusura();
                            }
                            //mapcampo == 20 ? "RICETTA_STOP"
                            boolean ricettaStop = false;
                            Query qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 1 AND maptiporeq = 0 AND mapcampo = 20");
                            qmp.apertura();
                            if (qmp.numeroRecord() > 0) {
                                Record r11 = qmp.successivo();
                                try {
                                    risposta = BusinessLogicOpc.lettura(client, r11.leggiStringa("mapopcuatag"), "" + r11.leggiIntero("mapopcnsindex"), r11.leggiIntero("maptype"), IdentifierType);
                                } catch (ServerListException ex) {
                                    break;
                                } catch (URISyntaxException ex) {
                                    break;
                                } catch (ServiceException ex) {
                                    break;
                                } catch (SecureIdentityException ex) {
                                    break;
                                } catch (IOException ex) {
                                    break;
                                } catch (StatusException ex) {
                                    break;
                                } catch (AddressSpaceException ex) {
                                    break;
                                }

                                if (r11.leggiIntero("maptype") == 1) {
                                    //String
                                } else if (r11.leggiIntero("maptype") == 2) {
                                    //int

                                } else if (r11.leggiIntero("maptype") == 3) {
                                    //bool
                                    ricettaStop = risposta.equals("true") ? true : false;
                                } else if (r11.leggiIntero("maptype") == 4) {
                                    //real
                                }
                                if (!ricettaStop) {
                                    rbatch = qbatch.successivo();
                                    continue;
                                }
                            }
                            qmp.chiusura();
                            // mapcampo == 11 ? "ID_ORDINE" 
                            qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 1 AND maptiporeq = 0 AND mapcampo = 11");
                            qmp.apertura();
                            if (qmp.numeroRecord() > 0) {
                                Record r11 = qmp.successivo();
                                int movdocIdletto = -1;
                                try {
                                    risposta = BusinessLogicOpc.lettura(client, r11.leggiStringa("mapopcuatag"), "" + r11.leggiIntero("mapopcnsindex"), r11.leggiIntero("maptype"), IdentifierType);
                                } catch (ServerListException ex) {
                                    break;
                                } catch (URISyntaxException ex) {
                                    break;
                                } catch (ServiceException ex) {
                                    break;
                                } catch (SecureIdentityException ex) {
                                    break;
                                } catch (IOException ex) {
                                    break;
                                } catch (StatusException ex) {
                                    break;
                                } catch (AddressSpaceException ex) {
                                    break;
                                }

                                if (r11.leggiIntero("maptype") == 1) {
                                    //String
                                } else if (r11.leggiIntero("maptype") == 2) {
                                    //int
                                    movdocIdletto = Formattazione.estraiIntero(risposta);
                                } else if (r11.leggiIntero("maptype") == 3) {
                                    //bool
                                } else if (r11.leggiIntero("maptype") == 4) {
                                    movdocIdletto = Formattazione.estraiIntero(risposta);
                                    //real
                                }
                                Query q1 = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out LEFT JOIN brewmatic_map ON mapmid = mapboutmid WHERE mapboutdocid = " + movdocIdletto + " AND (mapboutstato = 0 OR mapboutstato = 1) "
                                        + "AND mapboutmid = " + mId + " AND maptipoio = 1");
                                q1.apertura();
                                Record r1 = q1.successivo();
                                String mapmdocescont = "";
                                int mapmdocid = -1;
                                boolean trovato = false;
                                //effettuare la lettura completa sulla macchina
                                while (r1 != null) {
                                    try {
                                        risposta = BusinessLogicOpc.lettura(client, r1.leggiStringa("mapopcuatag"), "" + r1.leggiIntero("mapopcnsindex"), r1.leggiIntero("maptype"), IdentifierType);
                                    } catch (ServerListException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (URISyntaxException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (ServiceException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (SecureIdentityException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (IOException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (StatusException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    } catch (AddressSpaceException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    }

                                    if (risposta != null) {
                                        mapmdocescont = r1.leggiStringa("mapboutescont");
                                        mapmdocid = r1.leggiIntero("mapboutdocid");
                                        int mapid = r1.leggiIntero("mapid");
                                        r1.eliminaCampo("mapid");
                                        r1.eliminaCampo("mapdefval");
                                        r1.eliminaCampo("maptiporeq");
                                        r1.eliminaCampo("maptipoioreq");
                                        r1.insElem("mapvalue", risposta);
                                        r1.insElem("mapstato", (int) 1);
                                        r1.insElem("mapmdocescont", r1.leggiStringa("mapboutescont"));
                                        r1.insElem("mapmdocid", r1.leggiIntero("mapboutdocid"));
                                        r1.insElem("mapmapid", mapid);
                                        r1.eliminaCampo("mapboutid");
                                        r1.eliminaCampo("mapboutmid");
                                        r1.eliminaCampo("mapboutdocid");
                                        r1.eliminaCampo("mapboutescont");
                                        r1.eliminaCampo("mapboutdatains");
                                        r1.eliminaCampo("mapboutstato");
                                        r1.eliminaCampo("mapboutdocnum");
                                        r1.eliminaCampo("mapboutdatainizio");
                                        r1.eliminaCampo("mapboutorainizio");
                                        r1.eliminaCampo("mapboutdatafine");
                                        r1.eliminaCampo("mapboutorafine");
                                        //r1.eliminaCampo("mapboutqtaprod");
                                        QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "brewmatic_map_value_out", "mapid", r1);
                                        qins.esecuzione();
                                        trovato = true;
                                    }
                                    Thread.sleep(500);
                                    r1 = q1.successivo();
                                }
                                if (trovato) {
                                    aggiornaFineLavorazione(connAz, movdocIdletto, -1, rbatch.leggiIntero("mapboutid"), mId, mapmdocescont, mapmdocid, r.leggiIntero("mprotocollo"));
                                }
                                q1.chiusura();

                            } else {

                                // mapcampo == : mapcampo == 12 ? "NR_ORDINE_FINE"
                                // 26 NUMERO_ORDINE
                                qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 1 AND maptiporeq = 0 AND (mapcampo = 12 || mapcampo = 26)");
                                qmp.apertura();
                                if (qmp.numeroRecord() > 0) {

                                    Record r12 = qmp.successivo();
                                    int docnumLetto = -1;
                                    try {
                                        risposta = BusinessLogicOpc.lettura(client, r12.leggiStringa("mapopcuatag"), "" + r12.leggiIntero("mapopcnsindex"), r12.leggiIntero("maptype"), IdentifierType);
                                    } catch (ServerListException ex) {
                                        break;
                                    } catch (URISyntaxException ex) {
                                        break;
                                    } catch (ServiceException ex) {
                                        break;
                                    } catch (SecureIdentityException ex) {
                                        break;
                                    } catch (IOException ex) {
                                        break;
                                    } catch (StatusException ex) {
                                        break;
                                    } catch (AddressSpaceException ex) {
                                        break;
                                    }

                                    if (r12.leggiIntero("maptype") == 1) {
                                        //String
                                    } else if (r12.leggiIntero("maptype") == 2) {
                                        //int
                                        docnumLetto = Formattazione.estraiIntero(risposta);
                                    } else if (r12.leggiIntero("maptype") == 3) {
                                        //bool
                                    } else if (r12.leggiIntero("maptype") == 4) {
                                        //real
                                    }
                                    Query q1 = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out LEFT JOIN brewmatic_map ON mapmid = mapboutmid WHERE mapboutdocnum = " + docnumLetto + " AND (mapboutstato = 0 OR mapboutstato = 1) "
                                            + "AND mapboutmid = " + mId + " AND maptipoio = 1");
                                    q1.apertura();
                                    Record r1 = q1.successivo();
                                    String mapmdocescont = "";
                                    int mapmdocid = -1;
                                    boolean trovato = false;
                                    //effettuare la lettura completa sulla macchina
                                    while (r1 != null) {
                                        try {
                                            risposta = BusinessLogicOpc.lettura(client, r1.leggiStringa("mapopcuatag"), "" + r1.leggiIntero("mapopcnsindex"), r1.leggiIntero("maptype"), IdentifierType);
                                        } catch (ServerListException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (URISyntaxException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (ServiceException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (SecureIdentityException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (IOException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (StatusException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        } catch (AddressSpaceException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        }

                                        if (risposta != null) {
                                            mapmdocescont = r1.leggiStringa("mapboutescont");
                                            mapmdocid = r1.leggiIntero("mapboutdocid");
                                            int mapid = r1.leggiIntero("mapid");
                                            r1.eliminaCampo("mapid");
                                            r1.eliminaCampo("mapdefval");
                                            r1.eliminaCampo("maptiporeq");
                                            r1.eliminaCampo("maptipoioreq");
                                            r1.insElem("mapvalue", risposta);
                                            r1.insElem("mapstato", (int) 1);
                                            r1.insElem("mapmdocescont", r1.leggiStringa("mapboutescont"));
                                            r1.insElem("mapmdocid", r1.leggiIntero("mapboutdocid"));
                                            r1.insElem("mapmapid", mapid);
                                            r1.eliminaCampo("mapboutid");
                                            r1.eliminaCampo("mapboutmid");
                                            r1.eliminaCampo("mapboutdocid");
                                            r1.eliminaCampo("mapboutescont");
                                            r1.eliminaCampo("mapboutdatains");
                                            r1.eliminaCampo("mapboutstato");
                                            r1.eliminaCampo("mapboutdocnum");
                                            r1.eliminaCampo("mapboutdatainizio");
                                            r1.eliminaCampo("mapboutorainizio");
                                            r1.eliminaCampo("mapboutdatafine");
                                            r1.eliminaCampo("mapboutorafine");
                                            //r1.eliminaCampo("mapboutqtaprod");
                                            QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "brewmatic_map_value_out", "mapid", r1);
                                            qins.esecuzione();
                                            trovato = true;
                                        }
                                        Thread.sleep(500);
                                        r1 = q1.successivo();
                                    }
                                    if (trovato) {
                                        aggiornaFineLavorazione(connAz, -1, docnumLetto, rbatch.leggiIntero("mapboutid"), mId, mapmdocescont, mapmdocid, r.leggiIntero("mprotocollo"));
                                    }
                                    q1.chiusura();
                                }
                                qmp.chiusura();
                            }
                        }
                    } else if (r.leggiIntero("mprotocollo") == 2) {

                        String risposta = "";
                        session = BusinessLogicOpc.connection_v2(EnvJazz.ipServerOpc + r.leggiStringa("mip") + ":" + r.leggiIntero("mporta"));

                        if (session != null) {
                            //inizio lavorazione
                            if (stato != 1) {
                                //13 NR_ORDINE_INIZIO
                                //salva il momento in cui la macchina ha iniziato l'ordine salvando data e ora
                                Query qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 2 AND maptiporeq = 0 AND mapcampo = 13");
                                qmp.apertura();
                                if (qmp.numeroRecord() > 0) {

                                    Record r12 = qmp.successivo();
                                    int docnumLetto = -1;
                                    try {
                                        risposta = BusinessLogicOpc.lettura_v2(session, r12.leggiStringa("mapopcuatag"), r12.leggiIntero("mapopcnsindex"), IdentifierType);
                                    } catch (ServiceResultException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        break;
                                    }

                                    if (r12.leggiIntero("maptype") == 1) {
                                        //String
                                    } else if (r12.leggiIntero("maptype") == 2) {
                                        //int
                                        docnumLetto = Formattazione.estraiIntero(risposta);
                                    } else if (r12.leggiIntero("maptype") == 3) {
                                        //bool
                                    } else if (r12.leggiIntero("maptype") == 4) {
                                        //real
                                        docnumLetto = Formattazione.estraiIntero(risposta);
                                    }
                                    Query q1 = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out WHERE mapboutdocnum = " + docnumLetto + " AND mapboutstato = 0 "
                                            + "AND mapboutmid = " + mId);
                                    q1.apertura();
                                    Record r1 = q1.successivo();
                                    if (r1 != null) {
                                        aggiornaInizioLavorazione(connAz, batchId);
                                    }
                                } else {
                                    //mapcampo == 19 ? "RICETTA_START"
                                    //salva il momento in cui la macchina ha iniziato l'ordine salvando data e ora
                                    Query qm = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 2 AND maptiporeq = 0 AND mapcampo = 19");
                                    qm.apertura();
                                    boolean ricettaStart = false;
                                    if (qm.numeroRecord() > 0) {
                                        Record r11 = qm.successivo();
                                        try {
                                            risposta = BusinessLogicOpc.lettura_v2(session, r11.leggiStringa("mapopcuatag"), r11.leggiIntero("mapopcnsindex"), IdentifierType);
                                        } catch (ServiceResultException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                            break;
                                        }

                                        if (r11.leggiIntero("maptype") == 1) {
                                            //String
                                        } else if (r11.leggiIntero("maptype") == 2) {
                                            //int

                                        } else if (r11.leggiIntero("maptype") == 3) {
                                            //bool
                                            ricettaStart = risposta.equals("true") ? true : false;
                                        } else if (r11.leggiIntero("maptype") == 4) {
                                            //real
                                        }
                                        if (!ricettaStart) {
                                            rbatch = qbatch.successivo();
                                            qm.chiusura();
                                            qmp.chiusura();
                                            continue;
                                        }
                                        if (ricettaStart) {
                                            aggiornaInizioLavorazioneDaStartOrdine(connAz, batchId, mId, null, IdentifierType, 2, session);
                                        }
                                    }
                                    qm.chiusura();
                                }
                                qmp.chiusura();
                            }
                            //mapcampo == 20 ? "RICETTA_STOP"
                            boolean ricettaStop = false;
                            Query qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 2 AND maptiporeq = 0 AND mapcampo = 20");
                            qmp.apertura();
                            if (qmp.numeroRecord() > 0) {
                                Record r11 = qmp.successivo();
                                try {
                                    risposta = BusinessLogicOpc.lettura_v2(session, r11.leggiStringa("mapopcuatag"), r11.leggiIntero("mapopcnsindex"), IdentifierType);
                                } catch (ServiceResultException ex) {
                                    Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    break;
                                }

                                if (r11.leggiIntero("maptype") == 1) {
                                    //String
                                } else if (r11.leggiIntero("maptype") == 2) {
                                    //int

                                } else if (r11.leggiIntero("maptype") == 3) {
                                    //bool
                                    ricettaStop = risposta.equals("true") ? true : false;
                                } else if (r11.leggiIntero("maptype") == 4) {
                                    //real
                                }
                                if (!ricettaStop) {
                                    rbatch = qbatch.successivo();
                                    continue;
                                }
                            }
                            qmp.chiusura();
                            // mapcampo == 11 ? "ID_ORDINE" 
                            qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 2 AND maptiporeq = 0 AND mapcampo = 11");
                            qmp.apertura();
                            if (qmp.numeroRecord() > 0) {

                                Record r11 = qmp.successivo();
                                int movdocIdletto = -1;
                                String mapmdocescont = "";
                                int mapmdocid = -1;

                                try {
                                    risposta = BusinessLogicOpc.lettura_v2(session, r11.leggiStringa("mapopcuatag"), r11.leggiIntero("mapopcnsindex"), IdentifierType);
                                } catch (ServiceResultException ex) {
                                    Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                    break;
                                }

                                if (r11.leggiIntero("maptype") == 1) {
                                    //String
                                } else if (r11.leggiIntero("maptype") == 2) {
                                    //int
                                    movdocIdletto = Formattazione.estraiIntero(risposta);
                                } else if (r11.leggiIntero("maptype") == 3) {
                                    //bool
                                } else if (r11.leggiIntero("maptype") == 4) {
                                    //real
                                }
                                Query q1 = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out LEFT JOIN brewmatic_map ON mapmid = mapboutmid WHERE mapboutdocid = " + movdocIdletto + " AND (mapboutstato = 0 OR mapboutstato = 1) "
                                        + "AND mapboutmid = " + mId + " AND maptipoio = 1");
                                q1.apertura();
                                Record r1 = q1.successivo();
                                boolean trovato = false;
                                //effettuare la lettura completa sulla macchina
                                while (r1 != null) {
                                    try {
                                        risposta = BusinessLogicOpc.lettura_v2(session, r1.leggiStringa("mapopcuatag"), r1.leggiIntero("mapopcnsindex"), IdentifierType);
                                    } catch (ServiceResultException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);

                                    }

                                    if (risposta != null) {
                                        mapmdocescont = r1.leggiStringa("mapboutescont");
                                        mapmdocid = r1.leggiIntero("mapboutdocid");
                                        int mapid = r1.leggiIntero("mapid");
                                        r1.eliminaCampo("mapid");
                                        r1.eliminaCampo("mapdefval");
                                        r1.eliminaCampo("maptiporeq");
                                        r1.eliminaCampo("maptipoioreq");
                                        r1.insElem("mapvalue", risposta);
                                        r1.insElem("mapstato", (int) 1);
                                        r1.insElem("mapmdocescont", r1.leggiStringa("mapboutescont"));
                                        r1.insElem("mapmdocid", r1.leggiIntero("mapboutdocid"));
                                        r1.insElem("mapmapid", mapid);
                                        r1.eliminaCampo("mapboutid");
                                        r1.eliminaCampo("mapboutmid");
                                        r1.eliminaCampo("mapboutdocid");
                                        r1.eliminaCampo("mapboutescont");
                                        r1.eliminaCampo("mapboutdatains");
                                        r1.eliminaCampo("mapboutstato");
                                        r1.eliminaCampo("mapboutdocnum");
                                        r1.eliminaCampo("mapboutdatainizio");
                                        r1.eliminaCampo("mapboutorainizio");
                                        r1.eliminaCampo("mapboutdatafine");
                                        r1.eliminaCampo("mapboutorafine");
                                        //r1.eliminaCampo("mapboutqtaprod");
                                        QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "brewmatic_map_value_out", "mapid", r1);
                                        qins.esecuzione();
                                        trovato = true;
                                    }
                                    r1 = q1.successivo();
                                }

                                if (trovato) {
                                    //aggiornare lo stato sulla tabella brewmatic_batch_value_out
                                    aggiornaFineLavorazione(connAz, movdocIdletto, -1, rbatch.leggiIntero("mapboutid"), mId, mapmdocescont, mapmdocid, r.leggiIntero("mprotocollo"));
                                }
                                q1.chiusura();

                            } else {

                                // mapcampo == : mapcampo == 12 ? "NR_ORDINE_FINE"
                                // 26 NUMERO_ORDINE
                                qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + r.leggiIntero("mid") + " AND maptipoio = 1 AND mapprotocollo = 2 AND maptiporeq = 0 AND (mapcampo = 12 || mapcampo = 26)");
                                qmp.apertura();
                                if (qmp.numeroRecord() > 0) {

                                    Record r12 = qmp.successivo();
                                    int docnumLetto = -1;
                                    try {
                                        risposta = BusinessLogicOpc.lettura_v2(session, r12.leggiStringa("mapopcuatag"), r12.leggiIntero("mapopcnsindex"), IdentifierType);
                                    } catch (ServiceResultException ex) {
                                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);

                                    }
                                    if (r12.leggiIntero("maptype") == 1) {
                                        //String
                                    } else if (r12.leggiIntero("maptype") == 2) {
                                        //int
                                        docnumLetto = Formattazione.estraiIntero(risposta);
                                    } else if (r12.leggiIntero("maptype") == 3) {
                                        //bool
                                    } else if (r12.leggiIntero("maptype") == 4) {
                                        //real
                                    }
                                    Query q1 = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out LEFT JOIN brewmatic_map ON mapmid = mapboutmid WHERE mapboutdocnum = " + docnumLetto + " AND (mapboutstato = 0 OR mapboutstato = 1) "
                                            + "AND mapboutmid = " + mId + " AND maptipoio = 1");
                                    q1.apertura();
                                    Record r1 = q1.successivo();
                                    boolean trovato = false;
                                    String mapmdocescont = "";
                                    int mapmdocid = -1;
                                    //effettuare la lettura completa sulla macchina
                                    while (r1 != null) {
                                        try {
                                            risposta = BusinessLogicOpc.lettura_v2(session, r1.leggiStringa("mapopcuatag"), r1.leggiIntero("mapopcnsindex"), IdentifierType);
                                        } catch (ServiceResultException ex) {
                                            Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        if (risposta != null) {
                                            mapmdocescont = r1.leggiStringa("mapboutescont");
                                            mapmdocid = r1.leggiIntero("mapboutdocid");
                                            int mapid = r1.leggiIntero("mapid");
                                            r1.eliminaCampo("mapid");
                                            r1.eliminaCampo("mapdefval");
                                            r1.eliminaCampo("maptiporeq");
                                            r1.eliminaCampo("maptipoioreq");
                                            r1.insElem("mapvalue", risposta);
                                            r1.insElem("mapstato", (int) 1);
                                            r1.insElem("mapmdocescont", r1.leggiStringa("mapboutescont"));
                                            r1.insElem("mapmdocid", r1.leggiIntero("mapboutdocid"));
                                            r1.insElem("mapmapid", mapid);
                                            r1.eliminaCampo("mapboutid");
                                            r1.eliminaCampo("mapboutmid");
                                            r1.eliminaCampo("mapboutdocid");
                                            r1.eliminaCampo("mapboutescont");
                                            r1.eliminaCampo("mapboutdatains");
                                            r1.eliminaCampo("mapboutstato");
                                            r1.eliminaCampo("mapboutdocnum");
                                            r1.eliminaCampo("mapboutdatainizio");
                                            r1.eliminaCampo("mapboutorainizio");
                                            r1.eliminaCampo("mapboutdatafine");
                                            r1.eliminaCampo("mapboutorafine");
                                            //r1.eliminaCampo("mapboutqtaprod");
                                            QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "brewmatic_map_value_out", "mapid", r1);
                                            qins.esecuzione();
                                            trovato = true;
                                        }
                                        r1 = q1.successivo();
                                    }
                                    if (trovato) {
                                        aggiornaFineLavorazione(connAz, -1, docnumLetto, rbatch.leggiIntero("mapboutid"), mId, mapmdocescont, mapmdocid, r.leggiIntero("mprotocollo"));
                                    }
                                    q1.chiusura();
                                }
                            }
                            qmp.chiusura();
                        }
                    }
                }

                q.chiusura();
                rbatch = qbatch.successivo();
                Thread.sleep(500);
            }
            qbatch.chiusura();
//            if (client != null) {
//                client.disconnect();
//            }
            if (session != null) {
                try {
                    session.close();
                    session.closeAsync();
                } catch (ServiceResultException ex) {
                    Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            //connAz.chiudi();
            //connCom.chiudi();
        } catch (Exception ex) {
            Debug.output("Errore su JobLetturaSchedule");
            ex.printStackTrace();
        }
        Debug.output("JobLetturaSchedule OK");

    }

    private void aggiornaInizioLavorazione(Connection connAz, int batchId) {
        String oraInizio = Data.oraCorrente();
        String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        Record rins = new Record();
        rins.insElem("mapboutid", batchId);
        rins.insElem("mapboutdatainizio", oggi);
        rins.insElem("mapboutorainizio", oraInizio);
        rins.insElem("mapboutstato", 1);
        Record resitoagg = BusinessLogic.aggiornaRecord(connAz, "brewmatic_batch_value_out", "mapboutid", rins);
    }

    private void aggiornaFineLavorazione(Connection connAz, int movdocIdletto, int docnumLetto, int mapboutid, int mId, String mapmdocescont, int mapmdocid, int protocollo) {
        String mapboutdatainizio = "";
        String mapboutorainizio = "";
        String mapboutdatafine = "";
        String mapboutorafine = "";

        String oraFine = Data.oraCorrente();
        String dataFine = (new Data()).formatta(Data.AAAA_MM_GG, "-");
        if (movdocIdletto != -1) {
            //aggiornare lo stato sulla tabella brewmatic_batch_value_out
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, "UPDATE brewmatic_batch_value_out SET mapboutstato = 2 "//0 stato inviato,//1 stato iniziato, //2 stato finito
                    + " , mapboutdatafine = \"" + dataFine + "\" , mapboutorafine = \"" + oraFine + "\""
                    + " WHERE mapboutdocid = " + movdocIdletto + " AND mapboutid = " + mapboutid + " AND mapboutmid = " + mId);
            qa.esecuzione();
        } else if (docnumLetto != -1) {

            //aggiornare lo stato sulla tabella brewmatic_batch_value_out
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, "UPDATE brewmatic_batch_value_out SET mapboutstato = 2"
                    + " , mapboutdatafine = \"" + dataFine + "\" , mapboutorafine = \"" + oraFine + "\""
                    + " WHERE mapboutdocnum = " + docnumLetto + " AND mapboutid = " + mapboutid + " AND mapboutmid = " + mId);
            qa.esecuzione();
        }
        //forse controllare se esistono i tag di data e ora inizio e di data e ora fine cotta
        //9 DATA_INIZIO  10 DATA_FINE  
        //34 ORA_INIZIO  35 ORA_FINE
        //vengono salvati su brewmatic_map_value_out

        Query qm = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out WHERE mapboutdatafine = \"" + dataFine + "\" AND mapboutorafine = \"" + oraFine + "\""
                + " AND mapboutdocid = " + mapmdocid + " AND mapboutid = " + mapboutid + " AND mapboutmid = " + mId);
//        Query qm = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out WHERE mapboutdatafine = \"" + dataFine + "\" AND mapboutorafine = \"" + oraFine + "\""
//                + " AND mapboutdocnum = " + docnumLetto + " AND mapboutid = " + mapboutid + " AND mapboutmid = " + mId);
        qm.apertura();
        if (qm.numeroRecord() > 0) {
            Record rm = qm.successivo();
            mapboutdatainizio = rm.leggiStringa("mapboutdatainizio");
            mapboutorainizio = rm.leggiStringa("mapboutorainizio");
            mapboutdatafine = rm.leggiStringa("mapboutdatafine");
            mapboutorafine = rm.leggiStringa("mapboutorafine");
        }
        qm.chiusura();

        if (!mapboutdatainizio.equals("0000-00-00")) {
            //dataInizio
            Record rins = new Record();
            rins.insElem("mapvalue", mapboutdatainizio);
            rins.insElem("mapstato", (int) 1);
            rins.insElem("mapcampo", (int) 9);
            rins.insElem("mapmdocescont", mapmdocescont);
            rins.insElem("mapmdocid", mapmdocid);
            rins.insElem("mapmapid", (int) -1);//da verificare
            rins.insElem("mapmid", mId);
            rins.insElem("maptype", (int) 1);
            rins.insElem("maptipoio", (int) 1);
            rins.insElem("mapprotocollo", (int) protocollo);
            QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "brewmatic_map_value_out", "mapid", rins);
            qins.esecuzione();
        }
        if (!mapboutorainizio.equals("00:00")) {
            //ora inizio
            Record rins = new Record();
            rins.insElem("mapvalue", mapboutorainizio);
            rins.insElem("mapstato", (int) 1);
            rins.insElem("mapcampo", (int) 34);
            rins.insElem("mapmdocescont", mapmdocescont);
            rins.insElem("mapmdocid", mapmdocid);
            rins.insElem("mapmapid", (int) -1);//da verificare
            rins.insElem("mapmid", mId);
            rins.insElem("maptype", (int) 1);
            rins.insElem("maptipoio", (int) 1);
            rins.insElem("mapprotocollo", (int) protocollo);
            QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "brewmatic_map_value_out", "mapid", rins);
            qins.esecuzione();
        }
        //data fine
        Record rins = new Record();
        rins.insElem("mapvalue", mapboutdatafine);
        rins.insElem("mapstato", (int) 1);
        rins.insElem("mapcampo", (int) 10);
        rins.insElem("mapmdocescont", mapmdocescont);
        rins.insElem("mapmdocid", mapmdocid);
        rins.insElem("mapmapid", (int) -1);//da verificare
        rins.insElem("mapmid", mId);
        rins.insElem("maptype", (int) 1);
        rins.insElem("maptipoio", (int) 1);
        rins.insElem("mapprotocollo", (int) protocollo);
        QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "brewmatic_map_value_out", "mapid", rins);
        qins.esecuzione();

        //ora fine
        rins = new Record();
        rins.insElem("mapvalue", mapboutorafine);
        rins.insElem("mapstato", (int) 1);
        rins.insElem("mapcampo", (int) 35);
        rins.insElem("mapmdocescont", mapmdocescont);
        rins.insElem("mapmdocid", mapmdocid);
        rins.insElem("mapmapid", (int) -1);//da verificare
        rins.insElem("mapmid", mId);
        rins.insElem("maptype", (int) 1);
        rins.insElem("maptipoio", (int) 1);
        rins.insElem("mapprotocollo", (int) protocollo);
        qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "brewmatic_map_value_out", "mapid", rins);
        qins.esecuzione();
    }

    private void aggiornaInizioLavorazioneDaStartOrdine(Connection connAz, int batchId, int mid, UaClient client, int IdentifierType, int protocollo, SessionChannel session) {
        // mapcampo == 11 ? "ID_ORDINE"
        String risposta = "";
        Query qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + mid + " AND maptipoio = 0 AND mapprotocollo = " + protocollo + " AND maptiporeq = 0 AND mapcampo = 11");
        qmp.apertura();
        System.out.println("aggiornaInizioLavorazioneDaStartOrdine " + mid + " " + protocollo);
        Record r11 = qmp.successivo();
        if (r11 != null) {
            int movdocIdletto = -1;
            if (protocollo == 1) {
                try {
                    risposta = BusinessLogicOpc.lettura(client, r11.leggiStringa("mapopcuatag"), "" + r11.leggiIntero("mapopcnsindex"), r11.leggiIntero("maptype"), IdentifierType);
                } catch (ServerListException ex) {
                    return;
                } catch (URISyntaxException ex) {
                    return;
                } catch (ServiceException ex) {
                    return;
                } catch (SecureIdentityException ex) {
                    return;
                } catch (IOException ex) {
                    return;
                } catch (StatusException ex) {
                    return;
                } catch (AddressSpaceException ex) {
                    return;
                }

            } else if (protocollo == 2) {
                try {
                    risposta = BusinessLogicOpc.lettura_v2(session, r11.leggiStringa("mapopcuatag"), r11.leggiIntero("mapopcnsindex"), IdentifierType);
                } catch (ServiceResultException ex) {
                    Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
            }
            if (r11.leggiIntero("maptype") == 1) {
                //String
            } else if (r11.leggiIntero("maptype") == 2) {
                //int
                double rispdouble = Double.parseDouble(risposta);
                movdocIdletto = (int) rispdouble;
            } else if (r11.leggiIntero("maptype") == 3) {
                //bool
            } else if (r11.leggiIntero("maptype") == 4) {
                double rispdouble = Double.parseDouble(risposta);
                movdocIdletto = (int) rispdouble;
                //real
            }
            Query q1 = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out LEFT JOIN brewmatic_map ON mapmid = mapboutmid WHERE mapboutdocid = " + movdocIdletto + " AND mapboutstato = 0 "
                    + "AND mapboutmid = " + mid + " AND maptipoio = 0");
            q1.apertura();
            Record r1 = q1.successivo();
            if (r1 != null) {
                String oraInizio = Data.oraCorrente();
                String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                Record rins = new Record();
                rins.insElem("mapboutid", batchId);
                rins.insElem("mapboutdatainizio", oggi);
                rins.insElem("mapboutorainizio", oraInizio);
                rins.insElem("mapboutstato", 1);
                Record resitoagg = BusinessLogic.aggiornaRecord(connAz, "brewmatic_batch_value_out", "mapboutid", rins);
            }
            q1.chiusura();
        } else {
            // 26 NUMERO_ORDINE
            qmp = new Query(connAz, "SELECT * FROM brewmatic_map WHERE mapmid = " + mid + " AND maptipoio = 0 AND mapprotocollo = " + protocollo + " AND maptiporeq = 0 AND (mapcampo = 12 || mapcampo = 26)");
            qmp.apertura();
            Record r12 = qmp.successivo();
            if (r12 != null) {
                int docnumLetto = -1;
                if (protocollo == 1) {
                    try {
                        risposta = BusinessLogicOpc.lettura(client, r12.leggiStringa("mapopcuatag"), "" + r12.leggiIntero("mapopcnsindex"), r12.leggiIntero("maptype"), IdentifierType);
                    } catch (ServerListException ex) {
                        return;
                    } catch (URISyntaxException ex) {
                        return;
                    } catch (ServiceException ex) {
                        return;
                    } catch (SecureIdentityException ex) {
                        return;
                    } catch (IOException ex) {
                        return;
                    } catch (StatusException ex) {
                        return;
                    } catch (AddressSpaceException ex) {
                        return;
                    }
                } else if (protocollo == 2) {

                    try {
                        risposta = BusinessLogicOpc.lettura_v2(session, r12.leggiStringa("mapopcuatag"), r12.leggiIntero("mapopcnsindex"), IdentifierType);
                    } catch (ServiceResultException ex) {
                        Logger.getLogger(JobLetturaSchedule.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (r12.leggiIntero("maptype") == 1) {
                    //String
                } else if (r12.leggiIntero("maptype") == 2) {
                    //int
                    double rispdouble = Double.parseDouble(risposta);
                    docnumLetto = (int) rispdouble;
                } else if (r12.leggiIntero("maptype") == 3) {
                    //bool
                } else if (r12.leggiIntero("maptype") == 4) {
                    //real
                    double rispdouble = Double.parseDouble(risposta);
                    docnumLetto = (int) rispdouble;
                }
                Query q1 = new Query(connAz, "SELECT * FROM brewmatic_batch_value_out LEFT JOIN brewmatic_map ON mapmid = mapboutmid WHERE mapboutdocnum = " + docnumLetto + " AND mapboutstato = 0 "
                        + "AND mapboutmid = " + mid + " AND maptipoio = 0");
                q1.apertura();
                Record r1 = q1.successivo();
                if (r1 != null) {
                    String oraInizio = Data.oraCorrente();
                    String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                    Record rins = new Record();
                    rins.insElem("mapboutid", batchId);
                    rins.insElem("mapboutdatainizio", oggi);
                    rins.insElem("mapboutorainizio", oraInizio);
                    rins.insElem("mapboutstato", 1);
                    Record resitoagg = BusinessLogic.aggiornaRecord(connAz, "brewmatic_batch_value_out", "mapboutid", rins);
                }
                q1.chiusura();
            }

        }
        qmp.chiusura();
    }

}
