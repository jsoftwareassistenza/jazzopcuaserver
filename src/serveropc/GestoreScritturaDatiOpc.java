package serveropc;

import java.lang.*;
import java.sql.*;
import java.io.*;
import nucleo.DBInputStream;
import nucleo.DBOutputStream;
import nucleo.Data;
import nucleo.Debug;
import nucleo.FunzioniUtilita;
import nucleo.Record;

public class GestoreScritturaDatiOpc extends java.lang.Thread
{
    DBInputStream dbIn = null;
    DBOutputStream dbOut = null;
    String ipClient = "";

    public GestoreScritturaDatiOpc(DBInputStream dbIn, DBOutputStream dbOut, String ipClient)
    {
        this.dbIn = dbIn;
        this.dbOut = dbOut;
        this.ipClient = ipClient;

		//{{INIT_CONTROLS
		//}}
	}

    public void run()
    {
        String nf = "";
        String codicepost = "";
        String codicepostorig = "";
        try
        {
            Debug.output(" * INVIO DATI A POSTAZIONE");
            // lettura codice postazione
            codicepost = dbIn.leggiStringa();
            Debug.output(" - postazione " + codicepost);
            // ricerca file da inviare (quello modificato da pi� tempo)
            File f = new File("dati");
            String[] lf = f.list();
            long tmod = Long.MAX_VALUE;
            if (EnvOpc.connCom.isClosed())
                EnvOpc.connCom = DriverManager.getConnection(EnvOpc.connessioneJdbc + "trasfjazz", "root", "");
            for (int i = 0; i < lf.length; i++)
            {
                Statement st = EnvOpc.connCom.createStatement();
                ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM trasferimenti WHERE nomefile = \'" + lf[i] + "\' AND " +
                    "codicepostdest = \'" + codicepost + "\' AND trasferito = 0");
                rs.next();
                if (rs.getInt(1) > 0)
                {
                    File f2 = new File("dati" + File.separator + lf[i]);
                    if (f2.lastModified() <= tmod)
                    {
                        tmod = f2.lastModified();
                        nf = lf[i];
                    }
                }
                st.close();
            }

            codicepostorig = "";
            Statement st = EnvOpc.connCom.createStatement();
            ResultSet rs = st.executeQuery("SELECT codicepostorig FROM trasferimenti WHERE nomefile = \'" + nf + "\' AND codicepostdest = \'" + codicepost + "\'");
            if (rs.next())
                codicepostorig = rs.getString(1);
            st.close();
            dbOut.scriviStringa(codicepostorig);
            dbOut.flush();
            boolean invioOK = false;
            int ntent = 0;
            while (!invioOK && ntent < 20)
            {
                try
                {
                    ntent++;
                    Debug.output(" - Invio file in corso (tentativo " + ntent + ")...");
                    dbOut.scriviFile("dati", nf, dbIn);
                    dbOut.flush();
                    invioOK = true;
                }
                catch (Exception e)
                {
                    System.out.println("Errore su invio file a " + codicepost + ": " + e.getMessage());
                    invioOK = false;
                    ntent++;
                    Thread.currentThread().sleep(3000);
                }
            }
            if (invioOK)
            {
                Debug.output(" - file inviato");
                // aggiornamento tabella trasferimenti
                PreparedStatement pst = EnvOpc.connCom.prepareStatement("UPDATE trasferimenti SET trasferito = 1 WHERE codicepostdest = ? AND " +
                    "nomefile = ?");
                pst.clearParameters();
                pst.setString(1, codicepost);
                pst.setString(2, nf);
                pst.execute();
                pst.close();
                boolean term = false;
                // se destinatario = terminale chiude i movimenti xe
                if (codicepost.toLowerCase().startsWith("t") && (codicepost.length() == 3 || codicepost.length() == 4))
                {
                    Debug.output(" - chiusura movimenti XE");
                    term = true;
                    try
                    {
                        Connection connAmb = DriverManager.getConnection(EnvOpc.connessioneJdbc + "ambientejazz", EnvOpc.userDb, EnvOpc.pwdDb);
                        Statement ctst = connAmb.createStatement();
                        ResultSet ctrs = ctst.executeQuery("SELECT * FROM partrasfremoti_aziende WHERE codicepostorig = '" + codicepost + "'");
                        if (ctrs.next())
                        {
                            Connection connAz = DriverManager.getConnection(EnvOpc.connessioneJdbc + ctrs.getString("aggaz1"), EnvOpc.userDb, EnvOpc.pwdDb);
                            String es = "";
                            String oggi = (new Data()).formatta(Data.AAAA_MM_GG, "-");
                            st = connAmb.createStatement();
                            rs = st.executeQuery("SELECT * FROM esercizicont WHERE datainizio <= '" + oggi + "' AND datafine >= '" + oggi + "'");
                            if (rs.next())
                                es = rs.getString("escontabile");
                            st.close();
                            if (!es.equals(""))
                            {
                                Record rt = null;
                                st = connAz.createStatement();
                                rs = st.executeQuery("SELECT pedid FROM puntiemdoc WHERE pedcod = \"" + codicepost + "\"");
                                if (rs.next())
                                    rt = new Record(rs);
                                st.close();
                                if (rt != null)
                                {
                                    Record rd = null;
                                    st = connAz.createStatement();
                                    rs = st.executeQuery(
                                        "SELECT * FROM righepuntiemdoc INNER JOIN documenti ON rpeddocid = docid WHERE rpedpedid = " + rt.leggiIntero("pedid") + " AND doctipobolla = 4");
                                    if (rs.next())
                                        rd = new Record(rs);
                                    st.close();
                                    if (rd != null)
                                    {
                                        st = connAz.createStatement();
                                        rs = st.executeQuery(
                                            "SELECT movdocid FROM movimentidoc" + es +
                                            " LEFT JOIN movtrasfterm" + es + " ON movimentidoc" + es +
                                            ".movdocid = movtrasfterm" + es + ".mttmovdocid " +
                                            " LEFT JOIN documenti ON movimentidoc" + es + ".movdocdocumid = documenti.docid " +
                                            " WHERE movdocdocumid =  " + rd.leggiIntero("docid") +
                                            " AND movdocdtint2 <= \"" + (new Data()).formatta(Data.AAAA_MM_GG,"-") + "\" AND movdocdtint1 <= \"" + (new Data()).formatta(Data.AAAA_MM_GG,"-") + "\"" +
                                            " AND ISNULL(mttid) ORDER BY movdocdocumid,movdocdtint1,movdocdtint2,movdocnumint1,movdocnumint2");
                                        while (rs.next())
                                        {
                                            PreparedStatement pmst = connAz.prepareStatement(
                                                "INSERT INTO movtrasfterm" + es + " (mttmovdocid,mttstato) VALUES (?,?)");
                                            pmst.setInt(1, rs.getInt("movdocid"));
                                            pmst.setInt(2, 1);
                                            pmst.execute();
                                            pmst.close();
                                        }
                                        st.close();
                                    }
                                }
                            }
                            connAz.close();
                        }
                        ctst.close();
                        connAmb.close();
                    }
                    catch (Exception etr)
                    {
                        etr.printStackTrace();
                    }
                }                
                
                // controllo file inviato a tutte le destinazioni mappate
                pst = EnvOpc.connCom.prepareStatement("SELECT COUNT(*) FROM trasferimenti WHERE nomefile = ? AND trasferito = 0");
                pst.clearParameters();
                pst.setString(1, nf);
                rs = pst.executeQuery();
                rs.next();
                int num = rs.getInt(1);
                pst.close();
                if (num == 0)
                {
                    pst = EnvOpc.connCom.prepareStatement("DELETE FROM trasferimenti WHERE nomefile = ?");
                    pst.clearParameters();
                    pst.setString(1, nf);
                    pst.execute();
                    pst.close();
                    if (term && EnvOpc.backupdativsterm)
                    {
                        Debug.output(" - BACKUP file VS terminale");
                        File fdb = new File("backupdativsterm");
                        if (!fdb.exists())
                            fdb.mkdir();
                        FunzioniUtilita.copiaFile("dati" + File.separator + nf, "backupdativsterm" + File.separator + codicepost + "_" + nf);
                    }
                    File f2 = new File("dati" + File.separator + nf);
                    f2.delete();
                    Debug.output(" - file trasferito a tutte le destinazioni -> eliminato");
                }
                else
                    Debug.output(" - file ancora da trasferire a n." + num + " destinazioni");
                Debug.output(" * INVIO DATI A POSTAZIONE " + codicepost + " TERMINATO");
                Debug.output("");
            }
            else
            {
                Debug.output(" * INVIO DATI A POSTAZIONE " + codicepost + " FALLITO");
                Debug.output("");
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            try
            {
                //dbOut.scriviIntero(esito);
                dbOut.flush();
                dbOut.close();
                dbIn.close();
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }
    }
    private class DATIFileFilter implements FilenameFilter
    {
        public String codicepost = "";

        public boolean accept(File dir, String name)
        {
            Debug.output("dir:" + dir.getName());
            Debug.output("name:" + name);
            Debug.output("name:" + name);
            if (name.indexOf("dati_" + codicepost + "_") >= 0)
            {
                Debug.output("ok");
                return true;
            }
            else
            {
                Debug.output("Ko!");
                return false;
            }
        }
    }

	//{{DECLARE_CONTROLS
	//}}
}