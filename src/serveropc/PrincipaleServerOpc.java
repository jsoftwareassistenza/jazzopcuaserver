package serveropc;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import nucleo.EnvJazz;
import nucleo.Formattazione;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import static org.quartz.CronScheduleBuilder.dailyAtHourAndMinute;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

public class PrincipaleServerOpc {

    public PrincipaleServerOpc() {
    }

    public void lancia() {
        ServerOpc s = null;
        Connection conn = null;
        Statement st = null, st2 = null, st3 = null;
        ResultSet rs = null, rs2 = null, rs3 = null;

        // inizializza variabili di utilite'
        FunzioniUtilitaOpc.leggiSistemaOperativo();
        FunzioniUtilitaOpc.leggiSeparatoreDecimale();
        FunzioniUtilitaOpc.leggiDriverJdbc("serveropc.ini");
        FunzioniUtilitaOpc.leggiConnessioneJdbc("serveropc.ini");
        FunzioniUtilitaOpc.leggiHostServer("serveropc.ini");
        FunzioniUtilitaOpc.leggiUserDB("serveropc.ini");
        FunzioniUtilitaOpc.leggiPwdDB("serveropc.ini");
        FunzioniUtilitaOpc.leggiPortaServer("serveropc.ini");
        EnvOpc.azienda = FunzioniUtilitaOpc.leggiAzienda("serveropc.ini");
        FunzioniUtilitaOpc.leggiTipoLettura("serveropc.ini");

        // copia eseguibili mysql per backup da lib
//        File fmysqlx = new File("mysql.exe");
//        File fmysqllibx = new File("lib" + File.separator + "mysql.exe");
//        if (!fmysqlx.exists() && fmysqllibx.exists())
//        {
//            FunzioniUtilitaOpc.copiaFile("lib" + File.separator + "mysql.exe", "mysql.exe");
//        }
//        fmysqlx = new File("mysqldump.exe");
//        fmysqllibx = new File("lib" + File.separator + "mysqldump.exe");
//        if (!fmysqlx.exists() && fmysqllibx.exists())
//        {
//            FunzioniUtilitaOpc.copiaFile("lib" + File.separator + "mysqldump.exe", "mysqldump.exe");
//        }
        // lancia processo principale di gestione middleware e processo di server stampa
        s = new ServerOpc();
        s.setPriority(Thread.MAX_PRIORITY);
        s.start();

        // caricamento driver JDBC
        try {

            Driver d = (Driver) Class.forName(EnvOpc.driverJdbc).newInstance();
            System.out.println("=== Driver JDBC caricato...");
            System.out.println("=== Versione Driver JDBC: " + d.getMajorVersion() + "." + d.getMinorVersion());
            System.out.println("");
        } catch (Exception e) {
            System.out.println("Errore caricamento driver JDBC:" + e.getMessage());
        }

        // vede se server mysql attivo
        boolean databaseattivo = false;
        while (!databaseattivo) {
            try {
                // apre connessione a db mysql
                Connection c = DriverManager.getConnection(EnvOpc.connessioneJdbc + "mysql", EnvOpc.userDb, EnvOpc.pwdDb);
                databaseattivo = true;
                c.close();
            } catch (SQLException e) {
                //e.printStackTrace();
                System.out.println(e.getMessage());
                System.out.println("ATTENZIONE: Server Database Disattivo...");
            }
        }

        try {

            SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
            ServizioOpc.scheduler = schedFact.getScheduler();
            //        Statement ast = EnvOpc.connAmbiente.createStatement();

            try {
                EnvOpc.connAmbiente = DriverManager.getConnection(EnvOpc.connessioneJdbc
                        + "ambientejazz" + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);
                EnvOpc.connCom = DriverManager.getConnection(EnvOpc.connessioneJdbc
                        + "comunejazz" + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);
                EnvOpc.connAz = DriverManager.getConnection(EnvOpc.connessioneJdbc
                        + EnvOpc.azienda + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            ServizioOpc.scheduler.start();
        } catch (Exception ejcloud) {
            ejcloud.printStackTrace();
        }
        EnvJazz.hostServerOpc = EnvOpc.hostServer;//se non và salvare sul db
        EnvJazz.portaServerOpc = EnvOpc.portaServer;
        if (EnvOpc.tipoLettura.equals("")) {
            try {

                FunzioniUtilitaOpc.leggiSchedulazioneLetture("serveropc.ini");
                String gora = EnvOpc.oraSchedule;
//            if (int_minuti == 0) {
//
//                Statement pst = EnvOpc.connAz.createStatement();
//                ResultSet prs = pst.executeQuery("SELECT * FROM proprieta WHERE pprog = 'jcloud' AND pprop = 'giac_ora'");
//                if (prs.next()) {
//                    gora = prs.getString("pval");
//                }
//                pst.close();
//
//            }
                String azcorr = EnvOpc.azienda;

                // schedula job ricezione dati da terminali
                JobDetail job = newJob(JobLetturaRobot.class).withIdentity("jobletturarobot_" + azcorr, "group1")
                        .usingJobData("azienda", azcorr)
                        .usingJobData("server", EnvOpc.hostServer)
                        .build();
                TriggerBuilder tb = TriggerBuilder.newTrigger();
                tb.withIdentity("triggerletturarobot_" + azcorr, "group1");
                tb.startNow();

//            if (int_minuti > 0) {
//                SimpleScheduleBuilder sb = SimpleScheduleBuilder.simpleSchedule();
//                sb.withIntervalInSeconds(int_minuti * 60);
//                sb.repeatForever();
//                tb.withSchedule(sb);
//            } else {
                int h = Formattazione.estraiIntero(gora.substring(0, 2));
                int m = Formattazione.estraiIntero(gora.substring(3, 5));
                tb.withSchedule(dailyAtHourAndMinute(h, m));
                // }
                Trigger trig = tb.build();
                ServizioOpc.scheduler.scheduleJob(job, trig);
            } catch (Exception e) {

            }
            try {
                ServizioOpc.scheduler.start();
            } catch (SchedulerException ex) {
                Logger.getLogger(PrincipaleServerOpc.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (EnvOpc.tipoLettura.equals("SCHEDULE")) {
            try {

                FunzioniUtilitaOpc.leggiSchedulazioneLetture("serveropc.ini");
                String gora = EnvOpc.oraSchedule;
                String azcorr = EnvOpc.azienda;
                String peorainizio = "";
                String peorafine = "";
                String peazienda= "";
                int peintervallo = 0;
                int penumripet = 0;
                int petipolettura = 0;
                QueryInterrogazioneClient qRic = null;
                Record r;
                //Gestione unica macchina, per vedere come gestire multimacchine vedere il progetto
                //JazzBatchLettureOpcUa
                qRic = new QueryInterrogazioneClient(EnvOpc.connAmbiente, "SELECT * FROM pers_exec_opcua");
                qRic.apertura();
                for (int i = 0; i < qRic.numeroRecord(); i++) {
                    r = qRic.successivo();
                    peorainizio = r.leggiStringa("peorainizio");
                    peorafine = r.leggiStringa("peorafine");
                    peintervallo = r.leggiIntero("peintervallo");
                    peazienda = r.leggiStringa("peazienda");
                    penumripet = r.leggiIntero("penumripet");
                    petipolettura = r.leggiIntero("petipolettura"); //0 lettura sala cotta //1 lettura cisterna
                }
                qRic.chiusura();
                int int_minuti = 60; // da mettere un ora e peintervallo 20 e penumripet 3
                
                // schedula job 
                JobDetail job = newJob(JobLetturaSchedule.class).withIdentity("jobletturaschedule_" + azcorr, "group1")
                        .usingJobData("azienda", azcorr)
                        .usingJobData("server", EnvOpc.hostServer)
                        .usingJobData("peorainizio", peorainizio)
                        .usingJobData("peorafine", peorafine)
                        .usingJobData("peintervallo", peintervallo)
                        .usingJobData("peazienda", peazienda)
                        .usingJobData("penumripet", penumripet)
                        .usingJobData("petipolettura", petipolettura)
                        .build();
                TriggerBuilder tb = TriggerBuilder.newTrigger();
                tb.withIdentity("triggerletturaschedule_" + azcorr, "group1");
                tb.startNow();
                SimpleScheduleBuilder sb = SimpleScheduleBuilder.simpleSchedule();
                sb.withIntervalInSeconds((int) (int_minuti * 60));
                sb.repeatForever();
                tb.withSchedule(sb);
                Trigger trig = tb.build();
                ServizioOpc.scheduler.scheduleJob(job, trig);

            } catch (Exception e) {

            }
            try {
                ServizioOpc.scheduler.start();
            } catch (SchedulerException ex) {
                Logger.getLogger(PrincipaleServerOpc.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void main(String args[]) {
        (new PrincipaleServerOpc()).lancia();
    }

    public static String sostituisci(String orig, char c1, char c2) {
        String ris = orig;

        if (orig == null) {
            return null;
        }
        if (orig.length() == 0) {
            return orig;
        }
        int pos = 0;
        while ((pos = ris.indexOf(c1)) >= 0) {
            char[] c = {c2};
            ris = rimpiazza(ris, new String(c), pos);
        }
        return ris;
    }

    public static String rimpiazza(String orig, String s, int inizio) {
        String ris = "";

        if (orig == null) {
            return spazi(inizio) + s;
        } else if (orig.length() == 0) {
            return spazi(inizio) + s;
        } else if (orig.length() < inizio + 1) {
            return orig;
        } else if (inizio < 0) {
            return orig;
        } else if (s == null || s == "") {
            return orig;
        } else if (inizio + s.length() > orig.length()) {
            ris = orig.substring(0, inizio) + s;
            ris = ris.substring(0, orig.length());
            return ris;
        } else if (inizio + s.length() == orig.length()) {
            return orig.substring(0, inizio) + s;
        } else if (inizio == 0) {
            ris = s + orig.substring(s.length(), orig.length());
            return ris;
        } else {
            return orig.substring(0, inizio) + s + orig.substring(inizio + s.length());
        }
    }

    public static String spazi(int lung) {
        if (lung == 0) {
            return "";
        } else {
            return stringa(" ", lung);
        }
    }

    public static String stringa(String s, int lung) {
        String ris = "";
        for (int i = 0; i < lung; i++) {
            ris += s;
        }
        return ris;
    }

}
