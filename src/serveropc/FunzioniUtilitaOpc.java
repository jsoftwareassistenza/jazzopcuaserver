package serveropc;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.awt.*;
import java.text.*;
import javax.swing.*;
import java.lang.reflect.*;
import java.net.URI;
import nucleo.Formattazione;
import nucleo.funzStringa;

public class FunzioniUtilitaOpc {
    // The following function is a placeholder for control initialization.
    // You should call thiscrea function from a constructor or initialization function.

    public void vcInit() {
        //{{INIT_CONTROLS
        //}}
    }

    public static void leggiPortaServer(String fileIni) {
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            linea = conf.readLine();
            while (linea != null) {
                if (linea.length() >= 4) {
                    if (linea.startsWith("[PORTASERVER]")) {
                        linea = conf.readLine();
                        EnvOpc.portaServer = Formattazione.estraiIntero(linea);
                        break;
                    }
                }
                linea = conf.readLine();
            }
            conf.close();
        } catch (IOException e) {
            System.out.println("Errore lettura porta server");
            try {
                conf.close();
            } catch (IOException e2) {
            }
        }
    }

    public static final String leggiSistemaOperativo() {
        Properties propSistema = null;
        String s = "";

        propSistema = System.getProperties();
        s = propSistema.getProperty("os.name");
        EnvOpc.nomeSO = s;
        return s;
    }

    // chiamata alla partenza client: memorizza in Env nome host del server
    public static void leggiHostServer(String fileIni) {
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [HOST]
            while (true) {
                linea = conf.readLine();
                if (linea.length() >= 6) {
                    if (linea.substring(0, 6).equals("[HOST]")) {
                        break;
                    }
                }
            }
            linea = conf.readLine();
            linea = linea.substring(10);
            linea = funzStringa.rimuovi(linea, "\r");
            EnvOpc.hostServer = funzStringa.rimuovi(linea, "\n");
            conf.close();
        } catch (IOException e) {
            System.out.println("Errore lettura host");
            try {
                conf.close();
            } catch (IOException e2) {
            }
        }
    }

    public static void leggiDriverJdbc(String fileIni) {
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [DRIVER JDBC]
            while (true) {
                linea = conf.readLine();
                if (linea.length() >= 4) {
                    if (linea.startsWith("[DRIVER JDBC]")) {
                        break;
                    }
                }
            }
            linea = conf.readLine();
            EnvOpc.driverJdbc = funzStringa.rimuovi(
                    funzStringa.rimuovi(linea, "\r"), "\n");
            conf.close();
        } catch (IOException e) {
            System.out.println("Errore lettura driver jdbc:" + e.getMessage());
            try {
                conf.close();
            } catch (IOException e2) {
            }
        }
    }

    public static void leggiTipoLettura(String fileIni) {

        String linea = "";
        RandomAccessFile conf = null;
        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [ESERCIZIOIVA]
            while (true) {
                linea = conf.readLine();
                if (linea.length() >= 4) {
                    if (linea.startsWith("[TIPOLETTURA]")) {
                        break;
                    }
                }
            }
            linea = conf.readLine();
            conf.close();
            EnvOpc.tipoLettura = funzStringa.rimuovi(
                    funzStringa.rimuovi(linea, "\r"), "\n");
        } catch (IOException e) {
            System.out.println("Errore lettura tipo lettura " + e.getMessage());
            try {
                conf.close();
            } catch (IOException e2) {
            }
        }
    }

    public static void leggiConnessioneJdbc(String fileIni) {
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [CONNESSIONE JDBC]
            while (true) {
                linea = conf.readLine();
                if (linea.length() >= 4) {
                    if (linea.startsWith("[CONNESSIONE JDBC]")) {
                        break;
                    }
                }
            }
            linea = conf.readLine();
            EnvOpc.connessioneJdbc = funzStringa.rimuovi(
                    funzStringa.rimuovi(linea, "\r"), "\n");
            conf.close();
        } catch (IOException e) {
            System.out.println("Errore ");
            try {
                conf.close();
            } catch (IOException e2) {
            }
        }
    }

    public static void leggiUserDB(String fileIni) {
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [CONNESSIONE JDBC]
            boolean trovato = false;
            linea = conf.readLine();
            while (linea != null) {
                if (linea.length() >= 4 && linea.startsWith("[USERDB]")) {
                    trovato = true;
                    break;
                } else {
                    linea = conf.readLine();
                }
            }
            if (trovato) {
                linea = conf.readLine();
                EnvOpc.userDb = funzStringa.rimuovi(
                        funzStringa.rimuovi(linea, "\r"), "\n");
                if (EnvOpc.userDb.equals("")) {
                    EnvOpc.userDb = "root";
                }
            } else {
                EnvOpc.userDb = "root";
            }
            conf.close();
        } catch (IOException e) {
            EnvOpc.userDb = "root";
            try {
                conf.close();
            } catch (IOException e2) {
            }
        }
    }

    public static void leggiPwdDB(String fileIni) {
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [CONNESSIONE JDBC]
            boolean trovato = false;
            linea = conf.readLine();
            while (linea != null) {
                if (linea.length() >= 4 && linea.startsWith("[PWDDB]")) {
                    trovato = true;
                    break;
                } else {
                    linea = conf.readLine();
                }
            }
            if (trovato) {
                linea = conf.readLine();
                EnvOpc.pwdDb = funzStringa.rimuovi(
                        funzStringa.rimuovi(linea, "\r"), "\n");
            } else {
                EnvOpc.pwdDb = "";
            }
            conf.close();
        } catch (IOException e) {
            EnvOpc.pwdDb = "";
            try {
                conf.close();
            } catch (IOException e2) {
            }
        }
    }

    public static void leggiSeparatoreDecimale() {
        try {
            new Double("0.0");
            EnvOpc.dec = '.';
            EnvOpc.errDec = ',';
        } catch (Exception e) {
            EnvOpc.dec = ',';
            EnvOpc.errDec = '.';
        }
    }

    public static String cercaFile(String radice, String nomeFile) // radice senza separatore dopo (es. c:)
    {
        boolean trovato = false;
        Vector dirs = new Vector();

        File fRad = new File(radice);
        if (!fRad.isDirectory()) {
            return "";
        }
        dirs.addElement(radice);
        // lista files directory
        while (dirs.size() > 0 && !trovato) {
            // preleva directory dalla lista
            String dir = (String) dirs.elementAt(0);
            dirs.removeElementAt(0);
            String[] lf = (new File(dir)).list();
            for (int i = 0; i < lf.length; i++) {
                File f = new File(dir + File.separator + lf[i]);
                if (f.isFile() && nomeFile.equals(lf[i])) {
                    trovato = true;
                    return dir + File.separator + lf[i];
                } else if (f.isDirectory()) {
                    dirs.addElement(dir + File.separator + lf[i]);
                }
            }
        }
        return "";
    }

    public static void leggiPagineHelp(String percorsoHelp) {
        File f = new File(percorsoHelp);
        String[] listaFile = f.list();
        /*
         for (int i = 0; i < listaFile.length; i++)
         EnvOpc.listaHelp.insPagina(
         listaFile[i].substring(0, listaFile[i].length() - 4),
         "help" + File.separator + listaFile[i]);
         */
    }
    //{{DECLARE_CONTROLS
    //}}

    public static boolean copiaFile(String fOrig, String fDest) {
        byte[] b = new byte[8192];
        FileInputStream fis = null;
        FileOutputStream fos = null;
        boolean ok = true;
        int nBlocchi, nScarto;
        boolean scarto;
        int i;

        // controllo esistenza file di origine
        File fo = new File(fOrig);
        if (!fo.exists()) {
            return false;
        }
        nBlocchi = (int) fo.length() / 8192;
        scarto = ((fo.length() % 8192) != 0);
        nScarto = (int) (fo.length() % 8192);
        // copia effettiva
        try {
            fis = new FileInputStream(fOrig);
            fos = new FileOutputStream(fDest);
            for (i = 0; i < nBlocchi; i++) {
                fis.read(b);
                fos.write(b);
            }
            // copia blocco di scarto
            if (scarto) {
                byte[] sc = new byte[nScarto];
                fis.read(sc);
                fos.write(sc);
            }
            fis.close();
            fos.close();
        } catch (Exception e) {
            ok = false;
            e.printStackTrace();
        } finally {
            try {
                fis.close();
                fos.close();
            } catch (IOException e) {
                ok = false;
            }
            return ok;
        }
    }

    public static boolean copiaFile(String fOrig, String fDest, JProgressBar prog) {
        byte[] b = new byte[8192];
        FileInputStream fis = null;
        FileOutputStream fos = null;
        boolean ok = true;
        int nBlocchi, nScarto;
        boolean scarto;
        int i;

        // controllo esistenza file di origine
        File fo = new File(fOrig);
        if (!fo.exists()) {
            return false;
        }
        nBlocchi = (int) fo.length() / 8192;
        scarto = ((fo.length() % 8192) != 0);
        nScarto = (int) (fo.length() % 8192);
        if (prog != null) {
            prog.setValue(0);
            prog.setMaximum(nBlocchi);
        }
        // copia effettiva
        try {
            fis = new FileInputStream(fOrig);
            fos = new FileOutputStream(fDest);
            for (i = 0; i < nBlocchi; i++) {
                fis.read(b);
                fos.write(b);
                if (prog != null) {
                    prog.setValue(i);
                    prog.update(prog.getGraphics());
                }
            }
            // copia blocco di scarto
            if (scarto) {
                byte[] sc = new byte[nScarto];
                fis.read(sc);
                fos.write(sc);
            }
            fis.close();
            fos.close();
        } catch (Exception e) {
            ok = false;
        } finally {
            try {
                fis.close();
                fos.close();
            } catch (IOException e) {
                ok = false;
            }
            return ok;
        }
    }

    public static void copiaFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        while (true) {
            int readCount = in.read(buffer);
            if (readCount < 0) {
                break;
            }
            out.write(buffer, 0, readCount);
        }
    }

//    public static boolean copiaCartella(String cOrig, String cDest) {
//        boolean ok = true;
//
//        // controllo esistenza cartella di origine
//        File co = new File(cOrig);
//        if (!co.exists() || !co.isDirectory()) {
//            return false;
//        }
//        // crea cartella di destinazione
//        File cd = new File(cDest);
//        if (!cd.exists()) {
//            cd.mkdir();
//        }
//        // prende lista files in cartella origine e li copia
//        // se ci sono sottocartelle lancia questa procedura ricorsivamente
//        String[] lf = co.list();
//        for (int i = 0; i < lf.length && ok; i++) {
//            File f = new File(cOrig + File.separator + lf[i]);
//            if (f.isDirectory()) {
//                ok = copiaCartella(cOrig + File.separator + lf[i], cDest + File.separator + lf[i]);
//            } else {
//                ok = copiaFile(cOrig + File.separator + lf[i], cDest + File.separator + lf[i]);
//            }
//        }
//        return ok;
//    }
//
//    public static boolean copiaCartella(String cOrig, String cDest, JProgressBar progtot, JProgressBar progfile) {
//        return copiaCartella(cOrig, cDest, progtot, progfile, null);
//    }
//
//    public static boolean copiaCartella(String cOrig, String cDest, JProgressBar progtot, JProgressBar progfile, JLabel nomefile) {
//        boolean ok = true;
//
//        // controllo esistenza cartella di origine
//        File co = new File(cOrig);
//        if (!co.exists() || !co.isDirectory()) {
//            return false;
//        }
//        // crea cartella di destinazione
//        File cd = new File(cDest);
//        if (!cd.exists()) {
//            cd.mkdir();
//        }
//        // prende lista files in cartella origine e li copia
//        // se ci sono sottocartelle lancia questa procedura ricorsivamente
//        String[] lf = co.list();
//        if (progtot != null) {
//            progtot.setValue(0);
//            progtot.setMaximum(lf.length);
//        }
//        for (int i = 0; i < lf.length && ok; i++) {
//            File f = new File(cOrig + File.separator + lf[i]);
//            if (f.isDirectory()) {
//                ok = copiaCartella(cOrig + File.separator + lf[i], cDest + File.separator + lf[i], progtot, progfile, nomefile);
//            } else {
//                if (nomefile != null) {
//                    nomefile.setText(lf[i]);
//                    nomefile.update(nomefile.getGraphics());
//                }
//                if (progfile != null) {
//                    ok = copiaFile(cOrig + File.separator + lf[i], cDest + File.separator + lf[i], progfile);
//                } else {
//                    ok = copiaFile(cOrig + File.separator + lf[i], cDest + File.separator + lf[i]);
//                }
//            }
//            if (progtot != null) {
//                progtot.setValue(i);
//                progtot.update(progtot.getGraphics());
//            }
//        }
//        return ok;
//    }
    public static String leggiAzienda(String fileIni) {
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [AZIENDA]
            while (true) {
                linea = conf.readLine();
                if (linea.length() >= 4) {
                    if (linea.startsWith("[AZIENDA]")) {
                        break;
                    }
                }
            }
            linea = conf.readLine();
            conf.close();
            return funzStringa.rimuovi(
                    funzStringa.rimuovi(linea, "\r"), "\n");
        } catch (IOException e) {
            System.out.println("Errore lettura azienda: " + e.getMessage());
            try {
                conf.close();
            } catch (IOException e2) {
            }
            return "";
        }
    }

    public static String[] leggiEsercizioContabile(String fileIni) {
        String ris[] = new String[3];
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [ESERCIZIOCONT]
            while (true) {
                linea = conf.readLine();
                if (linea.length() >= 4) {
                    if (linea.startsWith("[ESERCIZIOCONT]")) {
                        break;
                    }
                }
            }
            linea = conf.readLine();
            ris[0] = funzStringa.rimuovi(funzStringa.rimuovi(linea, "\r"), "\n");
            linea = conf.readLine();
            ris[1] = funzStringa.rimuovi(funzStringa.rimuovi(linea, "\r"), "\n");
            linea = conf.readLine();
            ris[2] = funzStringa.rimuovi(funzStringa.rimuovi(linea, "\r"), "\n");
            conf.close();
            return ris;
        } catch (IOException e) {
            System.out.println("Errore lettura es.cont.: " + e.getMessage());
            try {
                conf.close();
            } catch (IOException e2) {
            }
            ris[0] = ris[1] = ris[2] = "";
            return ris;
        }
    }

    public static final String oraAttuale() {
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        return df.format(new java.util.Date());

    }

    public static Object creaOggetto(String nomeclasse, Object[] par) {
        Object o = null;
        try {
            Class cl = Class.forName(nomeclasse);
            // inizializza i tipi dei parametri
            Class[] cpar = new Class[par.length];
            if (par == null) {
                Constructor cc = cl.getConstructor(null);
                o = cc.newInstance(null);
            } else {
                for (int i = 0; i < par.length; i++) {
                    if (par[i].getClass().getName().endsWith("FormMenu") || par[i].getClass().getName().endsWith("FormPop")) {
                        cpar[i] = Class.forName("java.awt.Frame");
                    } else if (par[i].getClass().getName().endsWith("Boolean")) {
                        cpar[i] = Boolean.TYPE;
                    } else if (par[i].getClass().getName().endsWith("Integer")) {
                        cpar[i] = Integer.TYPE;
                    } else if (par[i].getClass().getName().endsWith("Double")) {
                        cpar[i] = Double.TYPE;
                    } else if (par[i].getClass().getName().endsWith("Float")) {
                        cpar[i] = Float.TYPE;
                    } else if (par[i].getClass().getName().endsWith("Connection")) {
                        cpar[i] = Class.forName("java.sql.Connection");
                    } else {
                        cpar[i] = par[i].getClass();
                    }
                }
                Constructor cc = cl.getConstructor(cpar);
                o = cc.newInstance(par);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return o;
    }

    public static Object invocaMetodo(Object o, String metodo, Object[] par) {
        Object ris = null;
        try {
            Method m = null;
            if (par == null) {
                m = o.getClass().getMethod(metodo, null);
                ris = m.invoke(o, null);
            } else {
                Class[] cpar = new Class[par.length];
                for (int i = 0; i < par.length; i++) {
                    if (par[i].getClass().getName().equals("FormMenu")) {
                        cpar[i] = Class.forName("java.awt.Frame");
                    } else if (par[i].getClass().getName().endsWith("Boolean")) {
                        cpar[i] = Boolean.TYPE;
                    } else if (par[i].getClass().getName().endsWith("Integer")) {
                        cpar[i] = Integer.TYPE;
                    } else if (par[i].getClass().getName().endsWith("Double")) {
                        cpar[i] = Double.TYPE;
                    } else if (par[i].getClass().getName().endsWith("Float")) {
                        cpar[i] = Float.TYPE;
                    } else {
                        cpar[i] = par[i].getClass();
                    }
                }
                m = o.getClass().getMethod(metodo, cpar);
                ris = m.invoke(o, par);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    public static final void lanciaManuale(String url) {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.OPEN)) {
            try {
                Desktop.getDesktop().browse(new URI(url));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static final Connection checkConnessione(Connection conn, String db) {
        boolean conn_ok = true;
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM mysql.db");
            st.close();
        } catch (Exception econn) {
            conn_ok = false;
        }
        if (!conn_ok) {
            try {
                conn = DriverManager.getConnection(EnvOpc.connessioneJdbc + db + EnvOpc.propJdbc, EnvOpc.userDb, EnvOpc.pwdDb);
                System.out.println("check connessione a " + db + ": connessione chiusa, riconnessione");
            } catch (Exception ex) {
            }
        }
        return conn;
    }

    public static int getColumnIndex(JTable table, String columnTitle) {
        int columnCount = table.getColumnCount();

        for (int column = 0; column < columnCount; column++) {
            if (table.getColumnName(column).equalsIgnoreCase(columnTitle)) {
                return column;
            }
        }

        return -1;
    }

    public static byte[] toByteArray(InputStream in) throws IOException {

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int len;

        // read bytes from the input stream and store them in buffer
        while ((len = in.read(buffer)) != -1) {
            // write bytes from the buffer into output stream
            os.write(buffer, 0, len);
        }

        return os.toByteArray();
    }

    public static double convertiByteInMByte(int nbyte) {
        return nbyte / 1024.0 / 1024.0;

    }

    public static void leggiSchedulazioneLetture(String fileIni) {
        String linea = "";
        RandomAccessFile conf = null;

        try {
            conf = new RandomAccessFile("env" + File.separator + fileIni, "r");
            // cerca riga [CONNESSIONE JDBC]
            boolean trovato = false;
            linea = conf.readLine();

            while (linea != null) {
                if (linea.length() >= 4 && linea.startsWith("[SCHEDULE LETTURA]")) {
                    trovato = true;
                    break;
                } else {
                    linea = conf.readLine();
                }
            }
            if (trovato) {
                linea = conf.readLine();
                EnvOpc.oraSchedule = funzStringa.rimuovi(
                        funzStringa.rimuovi(linea, "\r"), "\n");
            } else {
                EnvOpc.oraSchedule = "08:00";
            }
            conf.close();
        } catch (IOException e) {
            EnvOpc.oraSchedule = "08:00";
            try {
                conf.close();
            } catch (IOException e2) {
            }
        }
    }

}
