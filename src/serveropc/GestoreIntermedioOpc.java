package serveropc;

import com.prosysopc.ua.samples.client.SampleConsoleClient;
import java.lang.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import nucleo.DBInputStream;
import nucleo.DBOutputStream;
import nucleo.Debug;
import nucleo.EnvJazz;

public class GestoreIntermedioOpc extends java.lang.Thread {

    String ipClient = "";
    InputStream in = null;
    OutputStream out = null;
    DBInputStream oin = null;
    DBOutputStream oout = null;

    public GestoreIntermedioOpc(InputStream in, OutputStream out, String ipClient) {
        this.in = in;
        this.out = out;
        this.ipClient = ipClient;

    }

    public void run() {
        int com, codice;
        try {
            //Debug.output("GESTORE INTERMEDIO...");
            oin = new DBInputStream(in, 4096);
            oout = new DBOutputStream(out, 4096);
            // attende richiesta
            com = oin.leggiIntero();
            Debug.output("COM: " + com);
            if (com == EnvJazz.SCRITTURA_DATI_OPC) {
                String codiceRicetta = oin.leggiStringa();
                System.out.println("codiceRicetta in Scrittura");
                GestoreScritturaOpc(codiceRicetta);
            } else if (com == EnvJazz.LETTURA_DATI_OPC) {
                System.out.println("LETTURA_DATI_OPC");
                GestoreInformazioniOpc();
            }
        } catch (Exception e) {
        }
    }

    public void GestoreInformazioniOpc() {
        Vector vp = new Vector();
        try {
            // lettura codice postazione
            //devo cercare di far partire il server e fermarlo oopuure integrare il server con il serveropc
            Vector node = new Vector();
            Statement st = EnvOpc.connAz.createStatement();
            ResultSet rs = st.executeQuery("SELECT pval FROM proprieta WHERE pprog = \"par_robot\" ORDER BY pprop");
            while (rs.next()) {

                System.out.println("val" + rs.getString("pval"));
                node.addElement(rs.getString("pval"));
            }
            st.close();
            SampleConsoleClient d = new SampleConsoleClient(1, node, "");
            vp = d.leggiRisposta();

        } catch (Exception e) {
            System.out.println("Errore su invio elenco postazioni: " + e.getMessage());
            vp = new Vector();
        } finally {
            try {
                oout.scriviBloccoRecord(vp, oin);
                oout.flush();
                oout.close();
                oin.close();
            } catch (Exception e2) {
                System.out.println("Errore su invio elenco postazioni: " + e2.getMessage());
            }
        }
    }

    public void GestoreScritturaOpc(String codiceRicetta) {
        int ris = EnvJazz.SUCCESSO;
        try {
            // lettura codice postazione
            Vector node = new Vector();
            SampleConsoleClient d = new SampleConsoleClient(2, node, codiceRicetta);
            ris = d.leggiRispostaScrittura();

        } catch (Exception e) {
            System.out.println("Errore su invio elenco postazioni per scrittura: " + e.getMessage());
            ris = EnvJazz.FALLIMENTO;
        } finally {
            try {
                oout.scriviIntero(ris);
                oout.flush();
                oout.close();
                oin.close();
            } catch (Exception e2) {
            }
        }

    }

}
