package com.prosysopc.ua.samples.client;

import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.UaException;
import com.prosysopc.ua.client.AddressSpaceException;
import com.prosysopc.ua.client.ServerListException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.nodes.UaDataType;
import com.prosysopc.ua.nodes.UaInstance;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.nodes.UaType;
import com.prosysopc.ua.nodes.UaVariable;
import static com.prosysopc.ua.samples.client.SampleConsoleClient.print;
import com.prosysopc.ua.typedictionary.DynamicStructure;
import com.prosysopc.ua.types.opcua.AnalogItemType;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import nucleo.Formattazione;
import nucleo.Record;
import org.opcfoundation.ua.builtintypes.DataValue;
import org.opcfoundation.ua.builtintypes.DateTime;
import org.opcfoundation.ua.builtintypes.ExtensionObject;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.UnsignedInteger;
import org.opcfoundation.ua.builtintypes.UnsignedShort;
import org.opcfoundation.ua.builtintypes.Variant;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.core.Attributes;
import org.opcfoundation.ua.core.EUInformation;
import org.opcfoundation.ua.core.Identifiers;
import org.opcfoundation.ua.core.NodeClass;
import org.opcfoundation.ua.core.Range;
import org.opcfoundation.ua.encoding.DecodingException;
import org.opcfoundation.ua.transport.security.SecurityMode;
import org.opcfoundation.ua.utils.AttributesUtil;
import org.opcfoundation.ua.utils.MultiDimensionArrayUtils;

public class SampleUaClient {

    public boolean showReadValueDataType = false;
    public UaClient client;
    //String serverUrl = "opc.tcp://localhost:48020";
    public static String serverUrl = "opc.tcp://192.168.1.243:4840";
    public int operazione;
    public int esito;
    public Vector risposta;
    public Vector node;
    private String codiceRicetta;
    short nsIndex = 0;
    private String nodeIdValue;

    private SampleUaClient() {

    }

    private void readWriteTheAnswer(int value) throws UaException {
        short nsIndex = 0;
        String id = "RootNode_COMUNICAZIONE1_T_BOTT_LAVORATE";
        //String id = "RootNode_COMUNICAZIONE1_T_ALLARMI";
        NodeId nodeId = null;
        nodeId = new NodeId(nsIndex, id);
        // read metodo della libreria
        DataValue risposta = client.readValue(nodeId);
        UnsignedInteger attributeId = UnsignedInteger.valueOf(13);
        System.out.println(dataValueToString(nodeId, attributeId, risposta));
        System.out.println("Read node: " + nodeId);

    }

    private void testUnifiedAutomationServer() throws UaException, URISyntaxException, SecureIdentityException, IOException {

        // connect to server
        client = new UaClient(serverUrl);
        client.setSecurityMode(SecurityMode.NONE);
        initialize(client);
        client.connect();
        DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);
        System.out.println(value);
//        NodeId c = Identifiers.NodeId;
//        System.out.println(c);

        // read write single node
        String id = "RootNode_COMUNICAZIONE1_T_ALLARMI";
        id = "RootNode_COMUNICAZIONE1_T_BOTT_LAVORATE";
        //NodeId nodeId = new NodeId(0, "\"R_FORMATO\"");
        NodeId nodeId = new NodeId(nsIndex, id);
        //nodeId = new NodeId(17, "\"R_FORMATO\"");
        //UaNode node = null;
        value = client.readValue(nodeId);
        UaNode node = client.getAddressSpace().getNode(nodeId);
        String value2 = read(nodeId);
        int risultato = Formattazione.estraiIntero(value2);
        //read2(nodeId);

        // }
        // read metodo della libreria
        readWriteTheAnswer(0);

        System.out.println("Done!");
    }

    private void executeTests() {

        try {
            testUnifiedAutomationServer();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                client.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        SampleUaClient uaClient = new SampleUaClient();
        uaClient.executeTests();
    }

    public static void inizio(String[] args) throws Exception {

        //UaClient client = new UaClient("opc.tcp://192.168.1.243:4840");
        UaClient client = new UaClient(serverUrl);
        client.setSecurityMode(SecurityMode.NONE);
        initialize(client);
        client.connect();
        DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);
        System.out.println(value);
        client.disconnect();
    }

    protected static void initialize(UaClient client) throws SecureIdentityException, IOException, UnknownHostException {
        // *** Application Description is sent to the server
        ApplicationDescription appDescription = new ApplicationDescription();
        appDescription.setApplicationName(new LocalizedText("SimpleClient", Locale.ENGLISH));
        // 'localhost' (all lower case) in the URI is converted to the actual
        // host name of the computer in which the application is run
        appDescription.setApplicationUri("urn:localhost:UA:SimpleClient");
        appDescription.setProductUri("urn:prosysopc.com:UA:SimpleClient");
        appDescription.setApplicationType(ApplicationType.Client);

        final ApplicationIdentity identity = new ApplicationIdentity();
        identity.setApplicationDescription(appDescription);
        client.setApplicationIdentity(identity);
    }

    public String read(NodeId nodeId) throws ServiceException, StatusException {
        System.out.println("read node " + nodeId);
//    UnsignedInteger attributeId = readAttributeId();
        UnsignedInteger attributeId = UnsignedInteger.valueOf(13);
        DataValue value = client.readAttribute(nodeId, attributeId);
        System.out.println("read: -> " + dataValueToString(nodeId, attributeId, value));
        return (dataValue(nodeId, attributeId, value));

    }

    public UaDataType read2(NodeId nodeId) throws ServiceException, AddressSpaceException {

        UnsignedInteger[] attribs = {Attributes.DisplayName, Attributes.Value, Attributes.DataType};
        DataValue[] arrayOfValues = client.readAttributes(nodeId, attribs);
        String dataTypeString = arrayOfValues[2].getValue().toString();
        int dataTypeIdentifier = Integer.parseInt(dataTypeString.substring(dataTypeString.indexOf("=") + 1));
        UaDataType dt = (UaDataType) client.getAddressSpace().getType(new NodeId(0, dataTypeIdentifier));
        String test = dt.toString();
        System.out.println(dt.getJavaClass());
        return dt;
    }

    public void write(NodeId nodeId, String codiceRicetta) throws ServiceException, AddressSpaceException, StatusException {
        //UnsignedInteger attributeId = readAttributeId();
        UnsignedInteger attributeId = UnsignedInteger.valueOf(13);
        UaNode node = client.getAddressSpace().getNode(nodeId);
        System.out.println("Writing to node " + nodeId + " - " + node.getDisplayName().getText());

        // Find the DataType if setting Value - for other properties you must
        // find the correct data type yourself
        UaDataType dataType = null;
        if (attributeId.equals(Attributes.Value) && (node instanceof UaVariable)) {
            UaVariable v = (UaVariable) node;
            // Initialize DataType node, if it is not initialized yet
            if (v.getDataType() == null) {
                v.setDataType(client.getAddressSpace().getType(v.getDataTypeId()));
            }
            dataType = (UaDataType) v.getDataType();
            System.out.println("DataType: " + dataType.getDisplayName().getText());
        }

        print("Enter the value to write: ");
        //String value = "1";
        String value = codiceRicetta;
        print("Enter the value to write: " + codiceRicetta);
        //String value = readInput(true);//potrebbe essere qui il punto in cui si invia il codice ricetta

        Object convertedValue
                = dataType != null ? client.getAddressSpace().getDataTypeConverter().parseVariant(value, dataType) : value;
        boolean status = client.writeAttribute(nodeId, attributeId, convertedValue);
        if (status) {
            System.out.println("OK");

        } else {
            System.out.println("OK (completes asynchronously)");
        }

    }

    public String dataValueToString(NodeId nodeId, UnsignedInteger attributeId, DataValue value) {
        StringBuilder sb = new StringBuilder();
        sb.append("Node: ");
        sb.append(nodeId);
        sb.append(".");
        sb.append(AttributesUtil.toString(attributeId));
        sb.append(" | Status: ");
        sb.append(value.getStatusCode());
        if (value.getStatusCode().isNotBad()) {
            sb.append(" | Value: ");
            if (value.isNull()) {
                sb.append("NULL");
            } else {
                if (showReadValueDataType && Attributes.Value.equals(attributeId)) {
                    try {
                        UaVariable variable = (UaVariable) client.getAddressSpace().getNode(nodeId);
                        if (variable == null) {
                            sb.append("(Cannot read node datatype from the server) ");
                        } else {

                            NodeId dataTypeId = variable.getDataTypeId();
                            UaType dataType = variable.getDataType();
                            if (dataType == null) {
                                dataType = client.getAddressSpace().getType(dataTypeId);
                            }

                            Variant variant = value.getValue();
                            variant.getCompositeClass();
                            if (attributeId.equals(Attributes.Value)) {
                                if (dataType != null) {
                                    sb.append("(" + dataType.getDisplayName().getText() + ")");
                                } else {
                                    sb.append("(DataTypeId: " + dataTypeId + ")");
                                }
                            }
                        }
                    } catch (ServiceException e) {
                    } catch (AddressSpaceException e) {
                    }
                }
                final Object v = value.getValue().getValue();//valore finale
                if (value.getValue().isArray()) {
                    sb.append(MultiDimensionArrayUtils.toString(v));
                } else {
                    if (v instanceof ExtensionObject) {
                        // try reading the typedictionaries of the server in order to decode a custom Structure
                        try {
                            DynamicStructure ds = client.getTypeDictionary().decode((ExtensionObject) v);
                            sb.append(ds);
                        } catch (DecodingException e) {
                            sb.append(v);
                        }
                    } else {
                        sb.append(v);
                    }
                }
            }
        }
        sb.append(dateTimeToString(" | ServerTimestamp: ", value.getServerTimestamp(), value.getServerPicoseconds()));
        sb.append(dateTimeToString(" | SourceTimestamp: ", value.getSourceTimestamp(), value.getSourcePicoseconds()));
        return sb.toString();
    }

    public String dataValue(NodeId nodeId, UnsignedInteger attributeId, DataValue value) {
        String valore = "";
        StringBuilder sb = new StringBuilder();
        if (value.getStatusCode().isNotBad()) {
            if (value.isNull()) {
                sb.append("NULL");
            } else {
                if (showReadValueDataType && Attributes.Value.equals(attributeId)) {
                    try {
                        UaVariable variable = (UaVariable) client.getAddressSpace().getNode(nodeId);
                        if (variable == null) {
                            sb.append("(Cannot read node datatype from the server) ");
                        } else {

                            NodeId dataTypeId = variable.getDataTypeId();
                            UaType dataType = variable.getDataType();
                            if (dataType == null) {
                                dataType = client.getAddressSpace().getType(dataTypeId);
                            }

                            Variant variant = value.getValue();
                            variant.getCompositeClass();
                            if (attributeId.equals(Attributes.Value)) {
                                if (dataType != null) {
                                    sb.append(dataType.getDisplayName().getText());
                                } else {
                                    //sb.append("(DataTypeId: " + dataTypeId + ")");
                                }
                            }
                        }
                    } catch (ServiceException e) {
                    } catch (AddressSpaceException e) {
                    }
                }
                final Object v = value.getValue().getValue();//valore finale
                if (value.getValue().isArray()) {
                    sb.append(MultiDimensionArrayUtils.toString(v));
                    //valore = MultiDimensionArrayUtils.toString(v);
                } else {
                    if (v instanceof ExtensionObject) {
                        // try reading the typedictionaries of the server in order to decode a custom Structure
                        try {
                            DynamicStructure ds = client.getTypeDictionary().decode((ExtensionObject) v);
                            sb.append(ds);
                        } catch (DecodingException e) {
                            sb.append(v);
                        }
                    } else {
                        sb.append(v);
                    }
                }
            }
        }
        valore = sb.toString().trim();
        return valore;
    }

    /**
     * @param title
     * @param timestamp
     * @param picoSeconds
     * @return
     */
    public static String dateTimeToString(String title, DateTime timestamp, UnsignedShort picoSeconds) {
        if ((timestamp != null) && !timestamp.equals(DateTime.MIN_VALUE)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy MMM dd (zzz) HH:mm:ss.SSS");
            StringBuilder sb = new StringBuilder(title);
            sb.append(format.format(timestamp.getCalendar(TimeZone.getDefault()).getTime()));
            if ((picoSeconds != null) && !picoSeconds.equals(UnsignedShort.valueOf(0))) {
                sb.append(String.format("/%d picos", picoSeconds.getValue()));
            }
            return sb.toString();
        }
        return "";
    }

    public String getCurrentNodeAsString(UaNode node) {
        String nodeStr = "";
        String typeStr = "";
        String analogInfoStr = "";
        nodeStr = node.getDisplayName().getText();
        UaType type = null;
        if (node instanceof UaInstance) {
            type = ((UaInstance) node).getTypeDefinition();
        }
        typeStr = (type == null ? nodeClassToStr(node.getNodeClass()) : type.getDisplayName().getText());

        // This is the way to access type specific nodes and their
        // properties, for example to show the engineering units and
        // range for all AnalogItems
        if (node instanceof AnalogItemType) {

            AnalogItemType analogNode = (AnalogItemType) node;
            EUInformation units = analogNode.getEngineeringUnits();
            analogInfoStr = units == null ? "" : " Units=" + units.getDisplayName().getText();
            Range range = analogNode.getEURange();
            analogInfoStr
                    = analogInfoStr + (range == null ? "" : String.format(" Range=(%f; %f)", range.getLow(), range.getHigh()));

        }

        String currentNodeStr
                = String.format("*** Current Node: %s: %s (ID: %s)%s", nodeStr, typeStr, node.getNodeId(), analogInfoStr);
        return currentNodeStr;
    }

    /**
     * @param nodeClass
     * @return
     */
    public String nodeClassToStr(NodeClass nodeClass) {
        return "[" + nodeClass + "]";
    }

    public SampleUaClient(int operazione, Vector node, String codiceRicetta, String nodeIdValue, String serverUrl) throws ServerListException, URISyntaxException, SecureIdentityException, IOException, SessionActivationException, ServiceException, StatusException, AddressSpaceException {
        this.operazione = operazione;
        this.node = node;
        this.codiceRicetta = codiceRicetta;
        this.nodeIdValue = nodeIdValue;
        this.serverUrl = serverUrl;

        // connect to server
        client = new UaClient(serverUrl);
        client.setSecurityMode(SecurityMode.NONE);
        initialize(client);
        client.connect();
        DataValue valueServer = client.readValue(Identifiers.Server_ServerStatus_State);
        System.out.println(valueServer);

        if (operazione == 1) {
            risposta = lettura(nodeIdValue);
        } else if (operazione == 2) {
            esito = scrittura(codiceRicetta, nodeIdValue);
        } else {
            risposta = letturaMassiva(nodeIdValue, node);
        }

        client.disconnect();
    }

    public Vector letturaMassiva(String nodeIdValue, Vector node) throws ServerListException, URISyntaxException, ServiceException, SecureIdentityException, IOException, StatusException, AddressSpaceException {

        // read write single node
        //nodeIdValue = "RootNode_COMUNICAZIONE1_T_ALLARMI";
        //nodeIdValue = "\"DB201_OPC_WRITE\".\"Num_Pezzi_Prodotti\"";
        //nodeId = new NodeId(3, "\"DB201_OPC_WRITE\".\"Num_Pezzi_Prodotti\"");
        //nodeId = new NodeId(0, "RootNode_COMUNICAZIONE1_T_ALLARMI");
        NodeId nodeId = null;

        risposta = new Vector();
        for (int i = 0; i < node.size(); i++) {
            nodeId = new NodeId(nsIndex, node.get(i).toString());

            try {
                Record r = new Record();
                String value = read(nodeId);
                r.insElem("valore", Formattazione.estraiIntero(value));
                r.insElem("operazione", node.get(i).toString());
                risposta.add(r);
            } catch (ServiceException ex) {
                Logger.getLogger(SampleConsoleClient.class.getName()).log(Level.SEVERE, null, ex);
            } catch (StatusException ex) {
                Logger.getLogger(SampleConsoleClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return risposta;
    }

    public Vector lettura(String nodeIdValue) throws ServerListException, URISyntaxException, ServiceException, SecureIdentityException, IOException, StatusException, AddressSpaceException {

        // read write single node
        //nodeIdValue = "RootNode_COMUNICAZIONE1_T_ALLARMI";
        NodeId nodeId = null;

        risposta = new Vector();

        nodeId = new NodeId(nsIndex, nodeIdValue);
        //nodeId = new NodeId(3, "\"DB201_OPC_WRITE\".\"Num_Pezzi_Prodotti\"");
        try {
            Record r = new Record();
            String value = read(nodeId);
            r.insElem("valore", Formattazione.estraiIntero(value));
            r.insElem("operazione", nodeIdValue);
            risposta.add(r);
        } catch (ServiceException ex) {
            Logger.getLogger(SampleConsoleClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (StatusException ex) {
            Logger.getLogger(SampleConsoleClient.class.getName()).log(Level.SEVERE, null, ex);
        }

        return risposta;
    }

    public int scrittura(String codiceRicetta, String nodeIdValue) throws ServerListException, URISyntaxException, SecureIdentityException, IOException, ServiceException, StatusException {
        esito = 0;

        NodeId nodeId = new NodeId(nsIndex, nodeIdValue);
        try {
            write(nodeId, codiceRicetta);
            esito = 1;
        } catch (ServiceException ex) {
            Logger.getLogger(SampleConsoleClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AddressSpaceException ex) {
            Logger.getLogger(SampleConsoleClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (StatusException ex) {
            Logger.getLogger(SampleConsoleClient.class.getName()).log(Level.SEVERE, null, ex);
        }

        return esito;
    }

}
