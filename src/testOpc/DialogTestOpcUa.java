package testOpc;

/*
 * DialogConsegne.java
 *
 * Created on 20 maggio 2008, 12.42
 */
import com.prosysopc.ua.ApplicationIdentity;
import com.prosysopc.ua.SecureIdentityException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.UaException;
import com.prosysopc.ua.UserIdentity;
import com.prosysopc.ua.client.AddressSpaceException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.nodes.UaDataType;
import nucleo.industria40.BusinessLogicOpc;
import com.prosysopc.ua.nodes.UaNode;
import com.prosysopc.ua.nodes.UaType;
import com.prosysopc.ua.nodes.UaVariable;
import com.prosysopc.ua.typedictionary.DynamicStructure;
import nucleo.ConnessioneServer;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import nucleo.EnvJazz;
import nucleo.Formattazione;
import nucleo.ListaFrames;
import nucleo.PannelloTabellaSelezionaTuttiListener;
import nucleo.Record;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import nucleo.industria40.certs.ExampleKeys;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.opcfoundation.ua.application.Application;
import org.opcfoundation.ua.application.Client;
import org.opcfoundation.ua.application.SessionChannel;
import org.opcfoundation.ua.builtintypes.DataValue;
import org.opcfoundation.ua.builtintypes.DateTime;
import org.opcfoundation.ua.builtintypes.ExtensionObject;
import org.opcfoundation.ua.builtintypes.LocalizedText;
import org.opcfoundation.ua.builtintypes.NodeId;
import org.opcfoundation.ua.builtintypes.UnsignedInteger;
import org.opcfoundation.ua.builtintypes.UnsignedShort;
import org.opcfoundation.ua.builtintypes.Variant;
import org.opcfoundation.ua.common.ServiceResultException;
import org.opcfoundation.ua.core.ApplicationDescription;
import org.opcfoundation.ua.core.ApplicationType;
import org.opcfoundation.ua.core.Attributes;
import org.opcfoundation.ua.core.EndpointDescription;
import org.opcfoundation.ua.core.Identifiers;
import org.opcfoundation.ua.core.MessageSecurityMode;
import org.opcfoundation.ua.encoding.DecodingException;
import org.opcfoundation.ua.transport.security.CertificateValidator;
import org.opcfoundation.ua.transport.security.HttpsSecurityPolicy;
import org.opcfoundation.ua.transport.security.KeyPair;
import org.opcfoundation.ua.transport.security.SecurityMode;
import org.opcfoundation.ua.utils.AttributesUtil;
import org.opcfoundation.ua.utils.CertificateUtils;
import static org.opcfoundation.ua.utils.EndpointUtil.selectByMessageSecurityMode;
import static org.opcfoundation.ua.utils.EndpointUtil.selectByProtocol;
import org.opcfoundation.ua.utils.MultiDimensionArrayUtils;

/**
 *
 * @author svilupp
 */
public class DialogTestOpcUa extends javax.swing.JDialog implements PannelloTabellaSelezionaTuttiListener {

    private ConnessioneServer connAz = null, connCom = null;
    private Record rec = null;
    private Frame formGen;
    public boolean showReadValueDataType = false;
    private Vector risposta = new Vector();
    private String escontabile = "";
    private String azienda = "";
    private String esContabile = "";
    private int idVis;
    private int operazione;
    private ListaFrames lf;

    private Window padre;
    //public static String serverUrl = "opc.tcp://192.168.10.61:4840";
    public static String serverUrl = "";
    public int esito;
    public Vector node;
    //private String codiceRicetta;
    short nsIndex = 0;
    public UaClient client;
    //String serverUrl = "opc.tcp://localhost:48020";
    private SessionChannel session = null;

    /**
     * Creates new form DialogConsegne
     */
    public DialogTestOpcUa(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.getContentPane().setPreferredSize(new Dimension(600, 450));
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int dimSchermoX = d.width;
        int dimSchermoY = d.height;
        this.setLocation((dimSchermoX - this.getSize().width) / 2, (dimSchermoY - this.getSize().height) / 2);;

//        this.pack();
    }

    public DialogTestOpcUa(Frame parent, boolean modal, ConnessioneServer connAz,
            String azienda, Vector risposta) {
        this(parent, modal);
        this.setTitle("Lettura");
        this.connAz = connAz;
        this.formGen = parent;
        this.azienda = azienda;
        this.risposta = risposta;

    }

    public DialogTestOpcUa(Frame parent, boolean modal, ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, int operazione,
            String serverUrl, int nsIndex) {
        this(parent, modal);
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.operazione = operazione; //1 OPc Ua //2 OPC Ua Vers2
        this.serverUrl = serverUrl;
        this.nsIndex = (short) nsIndex;

        serverUrlText.setText(serverUrl);
        nsIndexText.setText("" + nsIndex);
        boolean conn = false;
        if (operazione == 1) {
            try {
                conn = testConnection();
            } catch (URISyntaxException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecureIdentityException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServiceException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (StatusException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (conn) {
                JOptionPane.showMessageDialog(formGen,
                        "Connesione Riuscita",
                        "Info", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

            } else {
                JOptionPane.showMessageDialog(this, "Test NON riuscito!!!", "Errore",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            }
        } else {
            String risposta = "";
            session = BusinessLogicOpc.connection_v2(serverUrl);
            vers2.setSelected(true);
            if (session != null) {
                System.out.println("session OK");
                JOptionPane.showMessageDialog(formGen,
                        "Connesione Riuscita",
                        "Info", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                try {
                    BusinessLogicOpc.disconnection_v2(session);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                JOptionPane.showMessageDialog(this, "Test NON riuscito!!!", "Errore",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;

            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pordfor = new javax.swing.JPanel();
        jLabel93 = new javax.swing.JLabel();
        jLabel95 = new javax.swing.JLabel();
        jLabel99 = new javax.swing.JLabel();
        jLabel81 = new javax.swing.JLabel();
        nsIndexText = new javax.swing.JTextField();
        serverUrlText = new javax.swing.JTextField();
        comandoText = new javax.swing.JTextField();
        testLettura = new javax.swing.JButton();
        ris = new javax.swing.JTextField();
        vers2 = new javax.swing.JCheckBox();
        testScrittura = new javax.swing.JButton();
        jLabel96 = new javax.swing.JLabel();
        comandoRicetta = new javax.swing.JTextField();
        IT_Numeric = new javax.swing.JCheckBox();
        jLabel27 = new javax.swing.JLabel();
        maptypeopc = new javax.swing.JComboBox();
        testLettura1 = new javax.swing.JButton();
        IdentifierLabel = new javax.swing.JLabel();
        mapreg1 = new javax.swing.JComboBox();
        testConnection = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Lettura Opc Ua");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        pordfor.setBackground(new java.awt.Color(255, 255, 255));
        pordfor.setMinimumSize(new java.awt.Dimension(600, 252));
        pordfor.setPreferredSize(new java.awt.Dimension(600, 268));
        pordfor.setLayout(null);

        jLabel93.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel93.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel93.setText("nsIndex");
        jLabel93.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pordfor.add(jLabel93);
        jLabel93.setBounds(4, 108, 96, 24);

        jLabel95.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel95.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel95.setText("Comando");
        jLabel95.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pordfor.add(jLabel95);
        jLabel95.setBounds(4, 72, 96, 24);

        jLabel99.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel99.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel99.setText("serverUrl");
        jLabel99.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pordfor.add(jLabel99);
        jLabel99.setBounds(16, 36, 84, 24);

        jLabel81.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel81.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel81.setText("Risultato");
        pordfor.add(jLabel81);
        jLabel81.setBounds(16, 236, 84, 24);

        nsIndexText.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        nsIndexText.setText("0");
        pordfor.add(nsIndexText);
        nsIndexText.setBounds(108, 108, 100, 24);

        serverUrlText.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        serverUrlText.setText("opc.tcp://192.168.172.200:4840");
        serverUrlText.setToolTipText("");
        pordfor.add(serverUrlText);
        serverUrlText.setBounds(108, 36, 340, 24);

        comandoText.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        pordfor.add(comandoText);
        comandoText.setBounds(108, 72, 340, 24);

        testLettura.setText("TEST LETTURA");
        testLettura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testLetturaActionPerformed(evt);
            }
        });
        pordfor.add(testLettura);
        testLettura.setBounds(388, 292, 140, 23);

        ris.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        pordfor.add(ris);
        ris.setBounds(108, 216, 252, 64);

        vers2.setBackground(new java.awt.Color(255, 255, 255));
        vers2.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        vers2.setText("Versione 2");
        pordfor.add(vers2);
        vers2.setBounds(388, 184, 88, 24);

        testScrittura.setText("TEST SCRITTURA");
        testScrittura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testScritturaActionPerformed(evt);
            }
        });
        pordfor.add(testScrittura);
        testScrittura.setBounds(388, 260, 140, 23);

        jLabel96.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel96.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel96.setText("Codice Ricetta");
        jLabel96.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pordfor.add(jLabel96);
        jLabel96.setBounds(4, 148, 96, 24);

        comandoRicetta.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        pordfor.add(comandoRicetta);
        comandoRicetta.setBounds(108, 148, 100, 24);

        IT_Numeric.setBackground(new java.awt.Color(255, 255, 255));
        IT_Numeric.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        IT_Numeric.setText("Numeric");
        pordfor.add(IT_Numeric);
        IT_Numeric.setBounds(236, 108, 88, 24);

        jLabel27.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel27.setText("Type");
        pordfor.add(jLabel27);
        jLabel27.setBounds(216, 148, 40, 24);

        maptypeopc.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        maptypeopc.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "----", "STRING", "UDINT", "BOOL", "REAL-FLOAT" }));
        pordfor.add(maptypeopc);
        maptypeopc.setBounds(260, 148, 124, 24);

        testLettura1.setText("CHIUDI");
        testLettura1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testLettura1ActionPerformed(evt);
            }
        });
        pordfor.add(testLettura1);
        testLettura1.setBounds(8, 316, 76, 23);

        IdentifierLabel.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        IdentifierLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        IdentifierLabel.setText("Identifier");
        IdentifierLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pordfor.add(IdentifierLabel);
        IdentifierLabel.setBounds(388, 148, 72, 24);

        mapreg1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        mapreg1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Int8", "Int16", "Int32", "Int64", "UInt32", "default" }));
        pordfor.add(mapreg1);
        mapreg1.setBounds(464, 148, 68, 24);

        testConnection.setText("TEST CONNECTION");
        testConnection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                testConnectionActionPerformed(evt);
            }
        });
        pordfor.add(testConnection);
        testConnection.setBounds(388, 224, 140, 23);

        getContentPane().add(pordfor, java.awt.BorderLayout.CENTER);

        setBounds(0, 0, 632, 384);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened

        //                        node.add("\"DB201_OPC_WRITE\".\"Ricetta_Caricata\"");
//        node.add("\"DB201_OPC_WRITE\".\"Num_Pezzi_Prodotti\"");
//        node.add("\"DB201_OPC_WRITE\".\"Tempo_di_Lavoro\"");
//        node.add("\"DB201_OPC_WRITE\".\"Allarmi_Macchina\"");
//        node.add("\"DB201_OPC_WRITE\".\"Consumo_della_finitura\"");
//        node.add("\"DB201_OPC_WRITE\".\"Consumo_della_spazzolatura\"");

    }//GEN-LAST:event_formWindowOpened


    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        if (vers2.isSelected()) {
            try {
                if (session != null) {
                    BusinessLogicOpc.disconnection_v2(session);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                client.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_formWindowClosed

    private void testLetturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testLetturaActionPerformed

        serverUrl = serverUrlText.getText();

        if (vers2.isSelected()) {
            session = BusinessLogicOpc.connection_v2(serverUrl);
            if (session != null) {
                System.out.println("session OK Lettura");
                testUnifiedAutomationServer_vers2();
            } else {
                System.out.println("session KO");
            }

        } else {
            try {
                testUnifiedAutomationServer();
            } catch (UaException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (URISyntaxException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecureIdentityException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (vers2.isSelected()) {
            session = connection(serverUrl);
            if (session != null) {
                System.out.println("session OK");
            }
        }
    }//GEN-LAST:event_testLetturaActionPerformed

    private void testScritturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testScritturaActionPerformed
        serverUrl = serverUrlText.getText();

        if (vers2.isSelected()) {
            session = BusinessLogicOpc.connection_v2(serverUrl);
            testUnifiedAutomationWtiteServer_vers2();
        } else {
            try {
                testUnifiedAutomationWtiteServer();
            } catch (UaException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (URISyntaxException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecureIdentityException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (vers2.isSelected()) {
            try {
                BusinessLogicOpc.disconnection_v2(session);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                client.disconnect();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_testScritturaActionPerformed

    private void testLettura1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testLettura1ActionPerformed
        dispose();
    }//GEN-LAST:event_testLettura1ActionPerformed

    private void testConnectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_testConnectionActionPerformed

        serverUrl = serverUrlText.getText();
        boolean conn = false;
        if (!vers2.isSelected()) {
            //if (operazione == 1) {
            System.out.println("Versione 1");
            try {
                conn = testConnection();
            } catch (URISyntaxException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecureIdentityException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ServiceException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            } catch (StatusException ex) {
                Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (conn) {
                JOptionPane.showMessageDialog(formGen,
                        "Connesione Riuscita",
                        "Info", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

            } else {
                JOptionPane.showMessageDialog(this, "Test NON riuscito!!!", "Errore",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            }
        } else {
            System.out.println("Versione 2");
            String risposta = "";
            session = BusinessLogicOpc.connection_v2(serverUrl);

            if (session != null) {
                System.out.println("session OK");
                JOptionPane.showMessageDialog(formGen,
                        "Connesione Riuscita",
                        "Info", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                try {
                    BusinessLogicOpc.disconnection_v2(session);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                JOptionPane.showMessageDialog(this, "Test NON riuscito!!!", "Errore",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;

            }
        }
    }//GEN-LAST:event_testConnectionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new DialogTestOpcUa(new javax.swing.JFrame(), true).show();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JCheckBox IT_Numeric;
    public javax.swing.JLabel IdentifierLabel;
    public javax.swing.JTextField comandoRicetta;
    public javax.swing.JTextField comandoText;
    public javax.swing.JLabel jLabel27;
    public javax.swing.JLabel jLabel81;
    public javax.swing.JLabel jLabel93;
    public javax.swing.JLabel jLabel95;
    public javax.swing.JLabel jLabel96;
    public javax.swing.JLabel jLabel99;
    public javax.swing.JComboBox mapreg1;
    public javax.swing.JComboBox maptypeopc;
    public javax.swing.JTextField nsIndexText;
    public javax.swing.JPanel pordfor;
    public javax.swing.JTextField ris;
    public javax.swing.JTextField serverUrlText;
    public javax.swing.JButton testConnection;
    public javax.swing.JButton testLettura;
    public javax.swing.JButton testLettura1;
    public javax.swing.JButton testScrittura;
    public javax.swing.JCheckBox vers2;
    // End of variables declaration//GEN-END:variables

    @Override
    public void azionemassiva(boolean stato) {
    }

    private void testUnifiedAutomationServer_vers2() {
        System.out.println("Inizio Lettura v2");
        String risposta = "";

        try {
            risposta = BusinessLogicOpc.lettura_v2(session, comandoText.getText(), Formattazione.estraiIntero(nsIndexText.getText()), IT_Numeric.isSelected() ? 1 : 0);
        } catch (ServiceResultException ex) {
            ex.printStackTrace();
            Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
        }

        ris.setText(risposta);

        System.out.println("Done!");
    }

    protected static void initialize(UaClient client) throws SecureIdentityException, IOException, UnknownHostException {
        // *** Application Description is sent to the server
        ApplicationDescription appDescription = new ApplicationDescription();
        appDescription.setApplicationName(new LocalizedText("SimpleClient", Locale.ENGLISH));
        // 'localhost' (all lower case) in the URI is converted to the actual
        // host name of the computer in which the application is run
        appDescription.setApplicationUri("urn:localhost:UA:SimpleClient");
        appDescription.setProductUri("urn:prosysopc.com:UA:SimpleClient");
        appDescription.setApplicationType(ApplicationType.Client);

        final ApplicationIdentity identity = new ApplicationIdentity();
        identity.setApplicationDescription(appDescription);
        client.setApplicationIdentity(identity);
    }

    public String read(NodeId nodeId) throws ServiceException, StatusException {
        System.out.println("read node " + nodeId);
//    UnsignedInteger attributeId = readAttributeId();
        UnsignedInteger attributeId = UnsignedInteger.valueOf(13);
        DataValue value = client.readAttribute(nodeId, attributeId);
        System.out.println("read: -> " + dataValueToString(nodeId, attributeId, value));
        return (dataValue(nodeId, attributeId, value));

    }

    public String dataValue(NodeId nodeId, UnsignedInteger attributeId, DataValue value) {
        String valore = "";
        StringBuilder sb = new StringBuilder();
        if (value.getStatusCode().isNotBad()) {
            if (value.isNull()) {
                sb.append("NULL");
            } else {
                if (showReadValueDataType && Attributes.Value.equals(attributeId)) {
                    try {
                        UaVariable variable = (UaVariable) client.getAddressSpace().getNode(nodeId);
                        if (variable == null) {
                            sb.append("(Cannot read node datatype from the server) ");
                        } else {

                            NodeId dataTypeId = variable.getDataTypeId();
                            UaType dataType = variable.getDataType();
                            if (dataType == null) {
                                dataType = client.getAddressSpace().getType(dataTypeId);
                            }

                            Variant variant = value.getValue();
                            variant.getCompositeClass();
                            if (attributeId.equals(Attributes.Value)) {
                                if (dataType != null) {
                                    sb.append(dataType.getDisplayName().getText());
                                } else {
                                    //sb.append("(DataTypeId: " + dataTypeId + ")");
                                }
                            }
                        }
                    } catch (ServiceException e) {
                    } catch (AddressSpaceException e) {
                    }
                }
                final Object v = value.getValue().getValue();//valore finale
                if (value.getValue().isArray()) {
                    sb.append(MultiDimensionArrayUtils.toString(v));
                    //valore = MultiDimensionArrayUtils.toString(v);
                } else {
                    if (v instanceof ExtensionObject) {
                        // try reading the typedictionaries of the server in order to decode a custom Structure
                        try {
                            DynamicStructure ds = client.getTypeDictionary().decode((ExtensionObject) v);
                            sb.append(ds);
                        } catch (DecodingException e) {
                            sb.append(v);
                        }
                    } else {
                        sb.append(v);
                    }
                }
            }
        }
        valore = sb.toString().trim();
        return valore;
    }

    /**
     * @param title
     * @param timestamp
     * @param picoSeconds
     * @return
     */
    public static String dateTimeToString(String title, DateTime timestamp, UnsignedShort picoSeconds) {
        if ((timestamp != null) && !timestamp.equals(DateTime.MIN_VALUE)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy MMM dd (zzz) HH:mm:ss.SSS");
            StringBuilder sb = new StringBuilder(title);
            sb.append(format.format(timestamp.getCalendar(TimeZone.getDefault()).getTime()));
            if ((picoSeconds != null) && !picoSeconds.equals(UnsignedShort.valueOf(0))) {
                sb.append(String.format("/%d picos", picoSeconds.getValue()));
            }
            return sb.toString();
        }
        return "";
    }

    public String dataValueToString(NodeId nodeId, UnsignedInteger attributeId, DataValue value) {
        StringBuilder sb = new StringBuilder();
        sb.append("Node: ");
        sb.append(nodeId);
        sb.append(".");
        sb.append(AttributesUtil.toString(attributeId));
        sb.append(" | Status: ");
        sb.append(value.getStatusCode());
        if (value.getStatusCode().isNotBad()) {
            sb.append(" | Value: ");
            if (value.isNull()) {
                sb.append("NULL");
            } else {
                if (showReadValueDataType && Attributes.Value.equals(attributeId)) {
                    try {
                        UaVariable variable = (UaVariable) client.getAddressSpace().getNode(nodeId);
                        if (variable == null) {
                            sb.append("(Cannot read node datatype from the server) ");
                        } else {

                            NodeId dataTypeId = variable.getDataTypeId();
                            UaType dataType = variable.getDataType();
                            if (dataType == null) {
                                dataType = client.getAddressSpace().getType(dataTypeId);
                            }

                            Variant variant = value.getValue();
                            variant.getCompositeClass();
                            if (attributeId.equals(Attributes.Value)) {
                                if (dataType != null) {
                                    sb.append("(" + dataType.getDisplayName().getText() + ")");
                                } else {
                                    sb.append("(DataTypeId: " + dataTypeId + ")");
                                }
                            }
                        }
                    } catch (ServiceException e) {
                    } catch (AddressSpaceException e) {
                    }
                }
                final Object v = value.getValue().getValue();//valore finale
                if (value.getValue().isArray()) {
                    sb.append(MultiDimensionArrayUtils.toString(v));
                } else {
                    if (v instanceof ExtensionObject) {
                        // try reading the typedictionaries of the server in order to decode a custom Structure
                        try {
                            DynamicStructure ds = client.getTypeDictionary().decode((ExtensionObject) v);
                            sb.append(ds);
                        } catch (DecodingException e) {
                            sb.append(v);
                        }
                    } else {
                        sb.append(v);
                    }
                }
            }
        }
        sb.append(dateTimeToString(" | ServerTimestamp: ", value.getServerTimestamp(), value.getServerPicoseconds()));
        sb.append(dateTimeToString(" | SourceTimestamp: ", value.getSourceTimestamp(), value.getSourcePicoseconds()));
        return sb.toString();
    }

    private boolean testConnection() throws URISyntaxException, SecureIdentityException, IOException, ServiceException, StatusException {
        boolean test = false;
        nsIndex = (short) Formattazione.estraiIntero(nsIndexText.getText());
        // connect to server
        client = new UaClient(serverUrl);
        client.setTimeout(60);
        client.setSecurityMode(SecurityMode.NONE);
        boolean credenziali = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "opz_credenziali", "" + idVis).equals("S");
        if (credenziali) {
            if (idVis != -1) {
                String userName = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "opz_credenziali_name", "" + idVis);
                String pws = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "opz_credenziali_pwd", "" + idVis);
//                String userName = "Supervision";
//        String pws = "2019_superBV";
                client.setUserIdentity(new UserIdentity(userName, pws));
                System.out.println("credenziali");
                System.out.println("userName" + userName);
                System.out.println("pws" + pws);
            } else {
                ris.setText("Salvare la macchina, credenziali impostate!");
                test = false;
                return test;
            }
        }
        initialize(client);
        client.connect();
        DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);
        System.out.println(value);
        ris.setText("" + value);
        if (client.isConnected()) {
            test = true;
        }
        return test;
    }

    private void testUnifiedAutomationServer() throws UaException, URISyntaxException, SecureIdentityException, IOException {

        nsIndex = (short) Formattazione.estraiIntero(nsIndexText.getText());
        // connect to server
//        client = new UaClient(serverUrl);
//        client.setSecurityMode(SecurityMode.NONE);
//        initialize(client);
//        client.connect();
//        DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);
//        System.out.println(value);

        // read write single node
        //String id = "RootNode_COMUNICAZIONE1_T_ALLARMI";
        // id = "RootNode_COMUNICAZIONE1_T_BOTT_LAVORATE";
        //NodeId n = new NodeId(3, "\"DB201_OPC_WRITE\".\"Ricetta_Caricata\"");
        if (client.isConnected()) {
            String id = comandoText.getText();
            System.out.println("COMANDO -> " + id);
            NodeId nodeId = null;
            //NodeId nodeId = new NodeId(nsIndex, id);
            if (IT_Numeric.isSelected()) {
                int foo = Integer.parseInt(id);
                nodeId = new NodeId(nsIndex, foo);
            } else {
                nodeId = new NodeId(nsIndex, id);
            }
            //nodeId = new NodeId(17, "\"R_FORMATO\"");
            DataValue value = client.readValue(nodeId);
            UaNode node = client.getAddressSpace().getNode(nodeId);
            String value2 = read(nodeId);
            int risultato = Formattazione.estraiIntero(value2);
            ris.setText("" + risultato);

            System.out.println("Done!");
        } else {
            System.out.println("Not Connect");
        }

    }

    private void testUnifiedAutomationWtiteServer() throws UaException, URISyntaxException, SecureIdentityException, IOException {

        nsIndex = (short) Formattazione.estraiIntero(nsIndexText.getText());
        // connect to server
//        client = new UaClient(serverUrl);
//        client.setSecurityMode(SecurityMode.NONE);
//        initialize(client);
//        client.connect();
//        DataValue value = client.readValue(Identifiers.Server_ServerStatus_State);
//        System.out.println(value);

        // read write single node
        //String id = "RootNode_COMUNICAZIONE1_T_ALLARMI";
        // id = "RootNode_COMUNICAZIONE1_T_BOTT_LAVORATE";
        //NodeId n = new NodeId(3, "\"DB201_OPC_WRITE\".\"Ricetta_Caricata\"");
        String id = comandoText.getText();

        NodeId nodeId = null;
        //NodeId nodeId = new NodeId(nsIndex, id);
        if (IT_Numeric.isSelected()) {
            int foo = Integer.parseInt(id);
            nodeId = new NodeId(nsIndex, foo);
        } else {
            nodeId = new NodeId(nsIndex, id);
        }

        boolean scrittura = false;
        try {
            scrittura = write(nodeId, comandoRicetta.getText().trim());

        } catch (ServiceException ex) {
            Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
        } catch (AddressSpaceException ex) {
            Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
        } catch (StatusException ex) {
            Logger.getLogger(DialogTestOpcUa.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (scrittura) {
            ris.setText("OK");

        } else {
            ris.setText("KO");

        }

    }

    public boolean write(NodeId nodeId, String codiceRicetta) throws ServiceException, AddressSpaceException, StatusException {
        //UnsignedInteger attributeId = readAttributeId();
        UnsignedInteger attributeId = UnsignedInteger.valueOf(13);
        UaNode node = client.getAddressSpace().getNode(nodeId);
        System.out.println("Writing to node " + nodeId + " - " + node.getDisplayName().getText());

        // Find the DataType if setting Value - for other properties you must
        // find the correct data type yourself
        UaDataType dataType = null;
        if (attributeId.equals(Attributes.Value) && (node instanceof UaVariable)) {
            UaVariable v = (UaVariable) node;
            // Initialize DataType node, if it is not initialized yet
            if (v.getDataType() == null) {
                v.setDataType(client.getAddressSpace().getType(v.getDataTypeId()));
            }
            dataType = (UaDataType) v.getDataType();
            System.out.println("DataType: " + dataType.getDisplayName().getText());
        }

        String value = codiceRicetta;
        System.out.println("Enter the value to write: " + codiceRicetta);
        //String value = readInput(true);//potrebbe essere qui il punto in cui si invia il codice ricetta

        Object convertedValue
                = dataType != null ? client.getAddressSpace().getDataTypeConverter().parseVariant(value, dataType) : value;
        boolean status = client.writeAttribute(nodeId, attributeId, convertedValue);
        return status;

    }

    private void testUnifiedAutomationWtiteServer_vers2() {

        if (session != null) {
            String codiceRicetta = comandoRicetta.getText().trim();
            nsIndex = (short) Formattazione.estraiIntero(nsIndexText.getText());
            int mapreg = -1;
            if (operazione == 2 && maptypeopc.getSelectedIndex() == 2) {
                mapreg = mapreg1.getSelectedIndex();
            }
            esito = BusinessLogicOpc.scrittura_v2(codiceRicetta, comandoText.getText(), nsIndex, session, maptypeopc.getSelectedIndex(), mapreg, IT_Numeric.isSelected() ? 1 : 0);
        }
        if (esito == EnvJazz.SUCCESSO) {
            ris.setText("OK");
        } else {
            ris.setText("KO");
        }
    }

    public static SessionChannel connection(String serverUrl) {
        System.out.println("Connection PROVA");
        Locale ENGLISH = Locale.ENGLISH;
        //////////////  CLIENT  //////////////
        // Create Client
        Application myApplication = new Application();
        Client myClient = new Client(myApplication);

        myApplication.addLocale(ENGLISH);
        myApplication.setApplicationName(new LocalizedText("Java Sample Client", Locale.ENGLISH));
        myApplication.setProductUri("urn:JavaSampleClient");

        CertificateUtils.setKeySize(1024); // default = 1024
        KeyPair pair = null;
        try {
            pair = ExampleKeys.getCert("SampleClient");
        } catch (ServiceResultException ex) {
            Logger.getLogger(BusinessLogicOpc.class.getName()).log(Level.SEVERE, null, ex);
        }
        myApplication.addApplicationInstanceCertificate(pair);

        // The HTTPS SecurityPolicies are defined separate from the endpoint securities
        myApplication.getHttpsSettings().setHttpsSecurityPolicies(HttpsSecurityPolicy.ALL);

        // Peer verifier
        myApplication.getHttpsSettings().setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        myApplication.getHttpsSettings().setCertificateValidator(CertificateValidator.ALLOW_ALL);

        // The certificate to use for HTTPS
//        KeyPair myHttpsCertificate = null;
//        try {
//            myHttpsCertificate = ExampleKeys.getHttpsCert("SampleClient");
//        } catch (ServiceResultException ex) {
//            Logger.getLogger(BusinessLogicOpc.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        myApplication.getHttpsSettings().setKeyPair(myHttpsCertificate);
        // Connect to the given uri
        SessionChannel mySession = null;
        try {

            serverUrl = serverUrl.replaceAll(" ", "%20");
            EndpointDescription[] endpoints = myClient.discoverEndpoints(serverUrl);
// Filter out all but opc.tcp protocol endpoints
            System.out.println("endpoints1 " + endpoints);
            endpoints = selectByProtocol(endpoints, "opc.tcp");
// Filter out all but Signed & Encrypted endpoints
            System.out.println("endpoints2 " + endpoints);
            endpoints = selectByMessageSecurityMode(endpoints, MessageSecurityMode.None);
            System.out.println("endpoints3 " + endpoints);
// Choose one endpoint
            if (endpoints.length == 0) {
                System.out.println("The server does not support insecure connections");
            }
            EndpointDescription endpoint = endpoints[0];
            System.out.println("endpoint***** " + endpoint);

            mySession = myClient.createSessionChannel(endpoint);
            System.out.println("mySession " + mySession);
        } catch (ServiceResultException ex) {
            System.out.println(ex);
        }
        if (mySession != null) {
            try {
                mySession.activate();
            } catch (ServiceResultException ex) {
                Logger.getLogger(BusinessLogicOpc.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        /*      /////////////  EXECUTE  //////////////      
        // Browse Root
        BrowseDescription browse = new BrowseDescription();
        browse.setNodeId( Identifiers.RootFolder );
        browse.setBrowseDirection( BrowseDirection.Forward );
        browse.setIncludeSubtypes( true );
        browse.setNodeClassMask( NodeClass.Object, NodeClass.Variable );
        browse.setResultMask( BrowseResultMask.All );
        BrowseResponse res3 = mySession.Browse( null, null, null, browse );             
        System.out.println(res3);

        // Read Namespace Array
        ReadResponse res5 = mySession.Read(
            null, 
            null, 
            TimestampsToReturn.Neither,                 
            new ReadValueId(Identifiers.Server_NamespaceArray, Attributes.Value, null, null ) 
        );
        String[] namespaceArray = (String[]) res5.getResults()[0].getValue().getValue();
        System.out.println(Arrays.toString(namespaceArray));

        // Read a variable
        ReadResponse res4 = mySession.Read(
            null, 
            500.0, 
            TimestampsToReturn.Source, 
            new ReadValueId(new NodeId(6, 1710), Attributes.Value, null, null ) 
        );      
        System.out.println(res4);

        res4 = mySession.Read(
            null, 
            500.0, 
            TimestampsToReturn.Source, 
            new ReadValueId(new NodeId(6, 1710), Attributes.DataType, null, null ) 
        );      
        System.out.println(res4);


        /////////////  SHUTDOWN  /////////////
        mySession.close();
        mySession.closeAsync();
        //////////////////////////////////////  

   // }
         */
        return mySession;
    }
}
